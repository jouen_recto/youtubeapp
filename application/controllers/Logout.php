<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		set_auth('login');
		$this->load->helper('url');
	
	}

	public function index()
	{
		$this->session->unset_userdata('user_data');
		redirect('login');
		
	}

	
	
}

?>