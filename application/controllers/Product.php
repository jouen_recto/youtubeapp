<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		set_auth('login');

		$this->load->helper('url');
		// $this->load->model('channel_m');
		// $this->load->model('video_m');
		// $this->load->model('category_m');
		// $this->load->model('favorite_m');
		// $this->load->model('note_m');
		// $this->load->model('insta_m');
		$this->load->model('product_m');
		$this->load->model('productquantity_m');
		$this->load->model('cron_m');

		//init pages
		$this->data['page_title'] = 'product';
		$this->data['flash_data'] = $this->session->flashdata();
		// shell_exec(sprintf("%s > %s 2>&1 & echo $! >> %s",'php index.php product test_cron','logs_cron.txt','logs_cron_id.txt'));
		$this->output->set_template('default');

		
	}

	public function index()
	{
		$user_data = $this->session->userdata('user_data')[0];
		//exec(sprintf("%s > %s 2>&1 & echo $! >> %s",'cron status','logs_cron.txt','logs_cron_id.txt'));
		// date_default_timezone_set('MST');
		// $today = strtotime(12 . ':00:00');
		// var_dump($today);
		// exit();
		$product = $this->product_m->getProduct(array('userId' => $user_data->id));

		$this->data['product'] = sizeof($product) == 0? false:$product;
		
		$this->load->js('assets/js/product/product-search.js');
		$this->load->view('product/index',$this->data);
	}

	public function searchPrudoct()
	{


		$user_data = $this->session->userdata('user_data')[0];

		$item_data = $this->product_m->getProduct(array('asin'=> $_POST['ASIN'], 'userId' => $user_data->id));
		
		if(sizeof($item_data) == 0)
		{	
			$add_to_database = true;

			//get quantity
			$qunatity_error = true;
			while ($qunatity_error) {
				$quantity_param = array(
				    "Service" => "AWSECommerceService",
				    "Operation" => "CartCreate",
				    "AWSAccessKeyId" => "AKIAJC6CZCPCDS6BUGMA",
				    "AssociateTag" => "asdasd1232",
				    "Item.1.ASIN" => $_POST['ASIN'],
				    "Item.1.Quantity" => 999
				);

				$quantity = amazom_api($quantity_param);
				if(property_exists($quantity,'Error') || is_null($quantity))
				{
					$qunatity_error = true;
				}else
				{
					$qunatity_error = false;
				}
			}


			//get error code
			$error_get_to_cart = false;
			if(property_exists($quantity->Cart->Request,'Errors'))
			{
				$error = $quantity->Cart->Request->Errors->Error;
				
				if($error->Code != "AWS.ECommerceService.InvalidQuantity")
				{
					$error_get_to_cart = true;
				}else
				{
					$item = $quantity->Cart->CartItems->CartItem;
				}
			}

			

			
			//get error code
			$error = $quantity->Cart->Request->Errors->Error;

			if($error->Code == "AWS.ECommerceService.ItemNotAccessible")
			{
				$flash_data = array(
					'status' =>  false,
					'msg' => 'This ASIN is not found or not accessible', 
				);

				$this->session->set_flashdata($flash_data);

				redirect('product');
			}


			//get image
			$img_param_error = true;
			while ($img_param_error) {
				$img_param = array(
				    "Service" => "AWSECommerceService",
				    "Operation" => "ItemLookup",
				    "AWSAccessKeyId" => "AKIAJC6CZCPCDS6BUGMA",
				    "AssociateTag" => "asdasd1232",
				    "ResponseGroup" => "Images",
				    "IdType" => 'ASIN',
				    "ItemId" => $_POST['ASIN'],
				    
				);
				//sleep(2);
				$img = amazom_api($img_param);
				if(property_exists($img,'Error') || is_null($img))
				{
					$img_param_error = true;
				}else
				{
					$img_param_error = false;
				}
			}
			
			$img_val = $img->Items->Item->MediumImage->URL;
			
			if(is_null($img_val))
			{
				$img_val = "none";
			}

			$get_item_data_error = true;
			while ($get_item_data_error) {
				$get_item_date = array(
				    "Service" => "AWSECommerceService",
				    "Operation" => "ItemLookup",
				    "AWSAccessKeyId" => "AKIAJC6CZCPCDS6BUGMA",
				    "AssociateTag" => "asdasd1232",
				    "IdType" => 'ASIN',
				    "ItemId" => $_POST['ASIN'],
				    
				);
				
				$lookup = amazom_api($get_item_date);

				if(property_exists($lookup,'Error') || is_null($lookup))
				{
					$get_item_data_error = true;
				}else
				{
					$get_item_data_error = false;
				}
			}

			$lookup_item = $lookup->Items->Item;
			
			if(property_exists($lookup->Items->Request, 'Errors'))
			{
				if($error_get_to_cart)
				{
					$add_to_database = false;
				}
				
			}

			$result_error_detail_url = true;
			while ($result_error_detail_url) {

				$params_detail_url = array(
				    "Service" => "AWSECommerceService",
				    "Operation" => "ItemLookup",
				    "AWSAccessKeyId" => "AKIAJC6CZCPCDS6BUGMA",
				    "AssociateTag" => "asdasd1232",
				    "ResponseGroup" => 'ItemAttributes',
				    "IdType" => 'ASIN',
				    "ItemId" => $_POST['ASIN'],
				    
				);

				$detail_url = amazom_api($params_detail_url);
				if(property_exists($detail_url,'Error'))
				{
					echo "error <br/><br/>";
				}else
				{
					$result_error_detail_url = false;
				}
			}

			$detailUrl = json_decode(json_encode($detail_url->Items->Item->DetailPageURL),true)[0];

			if($add_to_database)
			{
				if($error_get_to_cart)
				{
					$sellerNickname = "none";
					$price = "none";
					$quantity_value = "error";
					$asin_new_item = json_decode(json_encode($lookup_item->ASIN),true)[0];
					$title_new_item = json_decode(json_encode($lookup_item->ItemAttributes->Title),true)[0];
				}else
				{
					$sellerNickname = json_decode(json_encode($item->SellerNickname),true)[0];
					$price = json_encode($item->Price);
					$quantity_value = json_decode(json_encode($item->Quantity),true)[0];
					$asin_new_item = json_decode(json_encode($item->ASIN),true)[0];
					$title_new_item = json_decode(json_encode($item->Title),true)[0];
				}

				//set the item data
				$item_data = array(
					'asin' => $asin_new_item, 
					'userId' => $user_data->id,
					'sellerNickname' => $sellerNickname, 
					'title' => $title_new_item, 
					'price' => $price, 
					'img' => json_decode(json_encode($img_val),true)[0], 
					'detailUrl' => $detailUrl,
					'is_update' => true, 
					'is_track' => true, 
				);
				
				$item_id = $this->product_m->addProduct($item_data);

				$item_quantity = array(
					'productId' => $item_id,
					'quantity' => $quantity_value,
					'createdAt' => strtotime(12 . ':00:00'),
				);

				$qunatity_id = $this->productquantity_m->addQuantity($item_quantity);

				$item_data['id'] = $item_id;
				$item_data['quantity'] = $quantity_value;

			}else
			{
				$flash_data = array(
					'status' =>  false,
					'msg' => 'This ASIN is not found or not accessible', 
				);

				$this->session->set_flashdata($flash_data);

				redirect('product');
			}
		}else
		{
			$item_data = (array) $item_data[0];
			$today = strtotime(12 . ':00:00');

			$item_quantity = $this->productquantity_m->getQuantity(array('productId'=> $item_data['id'],'createdAt' => $today));

			$item_data['quantity'] = $item_quantity[0]->quantity;
		}

		$flash_data = array(
			'status' =>  true,
			'new_item' => $item_data, 
		);



		$this->session->set_flashdata($flash_data);

		redirect('product');
	}

	public function track($id = null)
	{
		// var_dump(sprintf("%s > %s 2>&1 & echo $! >> %s",'php index.php product test_cron','logs_cron.txt','logs_cron_id.txt'));
		// exit();
		
		// $output = shell_exec('crontab -l');
		// file_put_contents('crontab.txt', $output.'1 * * * * php public_html/youtubeApp/index.php product test_cron'.PHP_EOL);
		// echo exec('crontab -e 1 * * * * php public_html/youtubeApp/index.php product test_cron');

		if(is_null($id))
			redirect('product');
		
		$item = $this->product_m->getProduct(array('id'=> $id));

		if(sizeof($item) == 0)
		{
			$flash_data = array(
				'status' =>  false,
				'msg' => 'The page you request is not found', 
			);

			$this->session->set_flashdata($flash_data);

			redirect('product');
		}

		
		$item_quantity = $this->productquantity_m->getQuantity(array('productId'=> $item[0]->id));

		$item[0]->quantity = $item_quantity;

		$this->load->js('assets/js/product/product-track.js');

		$this->data['item'] = $item[0];
		$this->load->view('product/track',$this->data);
	}

	public function test_cron()
	{

		$this->output->unset_template();
		$today = strtotime(12 . ':00:00');
		// $tomorrow = strtotime('+1 day',$today);
		
		$item_quan = $this->productquantity_m->getQuantity(array('createdAt' => $today));

		if(sizeof($item_quan) <= 0)
		{
			
			$data = array(
				'is_update' => false, 

			);
			$where = array(
				'is_update' => true, 

			);

			$this->product_m->updateProduct($data,$where);
			$where = array(
				'is_update' => false, 

			);
			$product = $this->product_m->getProduct($where);
			

			foreach ($product as $index => $prod) {
				//get quantity
				$quantity_param = array(
				    "Service" => "AWSECommerceService",
				    "Operation" => "CartCreate",
				    "AWSAccessKeyId" => "AKIAJC6CZCPCDS6BUGMA",
				    "AssociateTag" => "asdasd1232",
				    "Item.1.ASIN" => $prod->asin,
				    "Item.1.Quantity" => 999
				);

				$quantity = amazom_api($quantity_param);
				$item = $quantity->Cart->CartItems->CartItem;

				$item_quantity = array(
					'productId' => $prod->id,
					'quantity' => json_decode(json_encode($item->Quantity),true)[0],
					'createdAt' => $today,
				);

				$qunatity_id = $this->productquantity_m->addQuantity($item_quantity);

				$data = array(
				'is_update' => true, 

				);
				$where = array(
					'id' => $prod->id, 

				);

				$this->product_m->updateProduct($data,$where);
				// echo "<pre>";
				// var_dump($prod->id);
				// echo "</pre>";
				
			}
		}

		


	}

	public function addBulk()
	{

		$this->load->js('assets/js/product/add-bulk-asin.js');
		$this->load->view('product/add-bulk',$this->data);
	}

	public function saveBulkProduct()
	{
		$this->output->unset_template();
		$user_data = $this->session->userdata('user_data')[0];
		$asin = json_decode($_POST['asin']);
		
		$data = array(
			'type' => 'save bulk product',
			'value' => json_encode($asin),
			'is_process' => false,
			'userId' => $user_data->id
		);

		$cron_id = $this->cron_m->addCronJob($data);

		cron('save_bulk_product',$cron_id);

		$flash_data = array(
			'status' =>  true,
			'msg' => 'The product ASIN is now processing' 
		);

		$this->session->set_flashdata($flash_data);

		redirect('product/addBulk');

	}

	public function deleteProduct()
	{
		$this->output->unset_template();

		$id = $_POST['prod_id'];

		$where = array(
			'id' => $id, 
		);

		$result = $this->product_m->deleteProduct($where);

	}


}

?>