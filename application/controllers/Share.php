<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Share extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		//is_login();
		$this->load->helper('url');
		$this->load->model('projectItem_m');
		$this->load->model('project_m');
		$this->load->model('channel_m');
		$this->load->model('ip_m');

		//init pages
		$this->data['page_title'] = 'Shared Project';
		// $this->data['no_nav'] = true;
		$this->output->set_template('public');

		
	}

	public function index($project_id = null)
	{
		
		redirect('login');
		
	}

	public function project($token = null)
	{
		if(!is_null($token))
		{
			$project = $this->project_m->getProject(array('share_token' => $token));

			if(sizeof($project) > 0)
			{
				$data = array(
					'projectId' => $project[0]->id,
					'limit' => 8,
					'offset' => 0,
				);

				$items = $this->projectItem_m->shareItem($data);

				$where = "";
				foreach ($items as $index_i => $i) {
					if($index_i < sizeof($items)-1)
					{
						$where .= "id = ".$i->channelId." OR ";
					}else
					{
						$where .= "id = ".$i->channelId;
					}
				}
				
				
				$channels = $this->channel_m->getChannel($where);	

				$this->data['data_form'] = $data;
				$this->data['projectId'] = $project[0]->id;
				$this->data['channels'] = $channels;
				
			}else
			{
				$this->data['projectId'] = false;
				$this->data['channels'] = false;
 			}

 			$this->load->js('assets/js/share/share.js');
			$this->load->view('share/project',$this->data);
		}else
		{
			redirect('login');
		}
	}

	public function loadChannelShare()
	{
		$this->output->unset_template();
		$data = array(
			'projectId' => $_POST['projectId'],
			'limit' => (int) $_POST['limit'],
			'offset' => (int) $_POST['offset'],
		);

		$items = $this->projectItem_m->shareItem($data);

		$where = "";
		foreach ($items as $index_i => $i) {
			if($index_i < sizeof($items)-1)
			{
				$where .= "id = ".$i->channelId." OR ";
			}else
			{
				$where .= "id = ".$i->channelId;
			}
		}
		
		// var_dump($where);
		
		$channels = $this->channel_m->getChannel($where);	

		// echo "<pre>";
		// var_dump($channels);
		// echo "</pre>";

		echo json_encode(array('channel' => $channels,'size' => (int) $_POST['offset'] + sizeof($channels)));
	}


	
}

?>