<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		set_auth('login');
		$this->load->helper('url');
		$this->load->model('channel_m');
		$this->load->model('video_m');
		$this->load->model('category_m');
		$this->load->model('project_m');
		$this->load->model('projectItem_m');

		//init pages
		$this->data['page_title'] = 'project';
		$this->output->set_template('default');
		
	}


	public function add()
	{
		$this->output->unset_template();
		
		//user
		$user = $this->session->userdata('user_data')[0];
		// $user->id = 3;

		// get post data
		$title = $_POST['title'];

		//add new project
		$data = array(
			'title' => $title,
			'userId' => $user->id, 
		);

		$projectId = $this->project_m->addProject($data);


	}

	public function getList()
	{
		$this->output->unset_template();
		
		//user
		$user = $this->session->userdata('user_data')[0];
		// $user->id = 3;

		$channelId = $_POST['channelId'];

		$project = $this->project_m->getProject(array('userId' => $user->id));

		if(sizeof($project) == 0)
		{
			$response = array('projects' => false );
		}else
		{
			foreach ($project as $index => $p) {
				$item = $this->projectItem_m->getItem(array('channelId' => $channelId, 'projectId' => $p->id ));
				
				if(sizeof($item) == 0)
					$project[$index]->in_list = false;
				else
					$project[$index]->in_list = true;

			}

			$response = array('projects' => $project );
		}

		echo json_encode($response);
	}

	public function addItem()
	{
		$this->output->unset_template();
	

		$projectId = $_POST['projectId'];
		$channelId = $_POST['channelId'];

		$data = array(
			'projectId' => $projectId,
			'channelId' => $channelId, 
		);

		$itemId = $this->projectItem_m->addItem($data);

		$project = $this->project_m->getProject(array('id' => $projectId));

		echo json_encode(array('title' => $project[0]->title ));

	}

	public function projectList()
	{
		//user
		$user = $this->session->userdata('user_data')[0];
		// $user->id = 3;


		$project = $this->project_m->getProject(array('userId' => $user->id));

		$this->data['project'] = $project;

		$this->load->js('assets/js/project/list.js');
		$this->load->js('assets/clipboard/dist/clipboard.min.js');
		$this->load->view('project/list',$this->data);
	}

	public function getProjectItems($passId = null)
	{
		$this->output->unset_template();
		$item_response = [];
		$channel_id_list = [];

		if(is_null($passId))
			$projectId = $_POST['id'];
		else
			$projectId = $passId;

		$project = $this->project_m->getProject(array('id' => $projectId));

		$items = $this->projectItem_m->getItem(array('projectId' => $projectId));

		$combine_ave = 0;
		$combine_cost = 0;
		foreach ($items as $index => $i) {
			$channel = $this->channel_m->getChannel(array('id' => $i->channelId ));

			if( sizeof($channel) != 0 ){
				$videos = $this->video_m->getChannelVideo($channel[0]->id,'viewCount','asc',20);
				$video_total_views_average = 0;
				if(sizeof($videos) != 0)
				{
					foreach ($videos as $video_index => $data) {
						$video_total_views_average += $data->viewCount;
					}

					$video_total_views_average = floor($video_total_views_average / sizeof($videos));
				}

				$item_data = array(
					'channelId' => $channel[0]->id,
					'title' => $channel[0]->title,
					'img' => json_decode($channel[0]->thumbnails)->default->url,
					'video' =>  $channel[0]->videoCount,
					'id' => $i->id,
					'email' => $channel[0]->email,
					'ave_view' => $video_total_views_average,
					'cost' => $channel[0]->cost,
					'is_active' => $i->is_active,
				);
				$combine_cost += $i->is_active? $channel[0]->cost:0;
				$combine_ave += $i->is_active? $video_total_views_average:0;

				array_push($item_response, $item_data);
				// array_push($channel_id_list, $channel[0]->id);
			}
		}

		$project[0]->combine_ave = $combine_ave;
		$project[0]->combine_cost = $combine_cost;
		$project[0]->estemated_sales = $combine_ave * $project[0]->cost_rate;
		$project[0]->estemated_gross = $project[0]->estemated_sales * $project[0]->sale_profit;
		$project[0]->estemated_net = $project[0]->estemated_gross - $combine_cost;


		$response = array('items' => $item_response, 'project' => $project[0] );
		// $response = array('items' => $channel_id_list, 'project' => $project[0] );

		if(is_null($passId))
			echo json_encode($response);
		else
			return $project[0];


	}

	public function deleteItem()
	{
		$this->output->unset_template();

		$itemId = $_POST['itemId'];

		$item = $this->projectItem_m->getItem(array('id' => $itemId));

		$this->projectItem_m->deleteItem($itemId);
		
		$project_details = $this->getProjectItems($item[0]->projectId);

		echo json_encode($project_details);

	}

	public function deleteProject()
	{
		$this->output->unset_template();

		$id = $_POST['id'];


		$this->project_m->deleteProject($id);

		$items = $this->projectItem_m->getItem(array('projectId' => $id));
		
		foreach ($items as $index => $i) {
			$this->projectItem_m->deleteItem($i->id);
		}
	}

	public function edit()
	{
		$this->output->unset_template();


		$option = "title";
		if(isset($_POST['option']))
			$option = $_POST['option'];

		$id = $_POST['projectId'];

		if($option == "title")
		{
			
			$text = $_POST['text'];

			$data = array(
				'title' => $text, 
			);
		}else if($option == "info")
		{
			$tp1 = $_POST['tp1'];
			$tp2 = $_POST['tp2'];
			$tp3 = $_POST['tp3'];
			$url = $_POST['url'];
			$cost_rate = $_POST['cost_rate'];
			$sale_profit = $_POST['sale_profit'];

			$data = array(
				'tp1' => $tp1, 
				'tp2' => $tp2, 
				'tp3' => $tp3, 
				'cost_rate' => floatval($cost_rate), 
				'sale_profit' => floatval($sale_profit), 
				'offer' => $url,
			);
		}
		
		
		$this->project_m->updateProject($data,$id);

		$project_details = $this->getProjectItems($id);

		echo json_encode($project_details);


	}

	public function duplicateProject()
	{
		$this->output->unset_template();

		$id = $_POST['projectId'];

		$project = $this->project_m->getProject(array('id' => $id));

		$data = array(
			'userId' => $project[0]->userId, 
			'title' => $project[0]->title, 
			'tp1' => $project[0]->tp1, 
			'tp2' => $project[0]->tp2, 
			'tp3' => $project[0]->tp3, 
			'offer' => $project[0]->offer, 
		);

		$new_project_id = $this->project_m->addProject($data);

		$project_item = $this->projectItem_m->getItem(array('projectId' => $id));

		foreach ($project_item as $index => $item) {
			$data = array(
				'projectId' => $new_project_id,
				'channelId' => $item->channelId, 
				'is_active' => $item->is_active,
			);

			$new_project_item_id = $this->projectItem_m->addItem($data);
		}

		$response = array(
			'id' => $new_project_id,
			'name' => $project[0]->title, 
		);

		echo json_encode($response);
	}

	public function projectUpdateItem($itemId = null, $is_active = null,$return = true)
	{
		$this->output->unset_template();
		if(is_null($itemId))
			$itemId = $_POST['item_id'];

		if(is_null($is_active))
			$is_active = $_POST['is_active']=="true"?true:false;

		$item = $this->projectItem_m->getItem(array('id' => $itemId));

		$data = array(
				'is_active' => $is_active, 
			);

		$this->projectItem_m->updateItem($data,$itemId);
		
		$project_details = $this->getProjectItems($item[0]->projectId);

		if($return)
			echo json_encode($project_details);
	}

	public function exportProject()
	{
		$this->output->unset_template();
		
		$item = $this->projectItem_m->getItem(array('projectId' => $_POST['projectId'] ));
		$project = $this->project_m->getProject(array('id' => $_POST['projectId']));

		$name_csv = str_replace(' ', '_',strtolower($project[0]->title)).".csv";
		$file_csv = fopen('assets/upload/'.$name_csv, "w");
		
		fputcsv($file_csv, array('title','Combined Average Views','Combined Cost','Estimated Sales','Estimated Gross', 'Estimated Net'));
		
		$combine_cost = 0;
		$combine_ave = 0;
		foreach ($item as $index => $i) {
			$channel = $this->channel_m->getChannel(array('id' => $i->channelId ));

			$videos = $this->video_m->getChannelVideo($channel[0]->id,'viewCount','asc',20);
			$video_total_views_average = 0;
			if(sizeof($videos) != 0)
			{
				foreach ($videos as $video_index => $data) {
					$video_total_views_average += $data->viewCount;
				}

				$video_total_views_average = floor($video_total_views_average / sizeof($videos));
			}

			
			$combine_cost += $i->is_active? $channel[0]->cost:0;
			$combine_ave += $i->is_active? $video_total_views_average:0;


		}
		$project[0]->estemated_sales = $combine_ave * $project[0]->cost_rate;
		$project[0]->estemated_gross = $project[0]->estemated_sales * $project[0]->sale_profit;
		$project[0]->estemated_net = $project[0]->estemated_gross - $combine_cost;
		fputcsv($file_csv, array($project[0]->title,$combine_ave,$combine_cost,$project[0]->estemated_sales,$project[0]->estemated_gross, $project[0]->estemated_net));


		fputcsv($file_csv, array('','','','',''));
		fputcsv($file_csv, array('','','','',''));
		fputcsv($file_csv, array('','','','',''));
		fputcsv($file_csv, array('','','','',''));
		fputcsv($file_csv, array('','','','',''));

		fputcsv($file_csv, array('title','email','no. videos','ave. views','cost','url'));
		foreach ($item as $index => $p_item) {
			$channel = $this->channel_m->getChannel(array('id' => $p_item->channelId ));

			fputcsv($file_csv, array($channel[0]->title,$channel[0]->email,$channel[0]->videoCount,$channel[0]->ave_view,$channel[0]->cost,"https://www.youtube.com/channel/".$channel[0]->yId));
		}

		



		echo str_replace(' ', '_',strtolower($project[0]->title)).".csv";


	}

	public function downloadCsv($name = null)
	{
		$this->output->unset_template();
		if(!is_null($name))
		{
			$file_url = base_url().'assets/upload/'.$name;
			header('Content-Type: application/octet-stream');
			header("Content-Transfer-Encoding: Binary"); 
			header("Content-disposition: attachment; filename=\"" . basename($file_url) . "\""); 
			readfile($file_url); // do the double-download-dance (dirty but worky)
		}else
		{
			return false;
		}
	}

	public function UpdateBulkItems()
	{
		$this->output->unset_template();
		// echo "UpdateBulkItems";
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script type="text/javascript" src='<?= base_url(); ?>assets/vendor/js/jquery.min.js'></script>
<script type="text/javascript" src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>
<style type="text/css">
	body{
		background-color: transparent;
	}
</style>
<div class="progress">
  <div class="progress-bar" role="progressbar" id="loading-bar-bulk" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
</div>
<?php
	
		ini_set('max_execution_time', 0);
		$method = $_POST['method'];
		$channel_id_arr = json_decode($_POST['channel_ids']);
		$projectId = $_POST['projectId'];

		if($method == "activate")
		{
			$where = "";
			foreach ($channel_id_arr as $index => $channel_id) {
				//$this->projectUpdateItem($channel_id,true,false);
				
				if($index == sizeof($channel_id_arr)-1)
				{
					$where .= "id=".$channel_id;
				}else
				{
					$where .= "id=".$channel_id." OR ";
				}

				$index_num = $index + 1;
				$percent_load = floor(($index_num / sizeof($channel_id_arr)) * 100);
				echo "<script>$('.progress').hide();</script>";
				echo '<div class="progress">';
				echo '<div class="progress-bar" role="progressbar" id="loading-bar-bulk" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: '.$percent_load.'%" >'.$percent_load.'%</div>';
				//echo '<div class="progress">';
				echo '</div>';
				usleep(25000);
			}
			$data = array(
				'is_active' => true, 
			);

			$this->projectItem_m->updateItem($data,$where,true);

		}else if($method == "deactivate")
		{
			$where = "";
			foreach ($channel_id_arr as $index => $channel_id) {
				// $this->projectUpdateItem($channel_id,false,false);

				if($index == sizeof($channel_id_arr)-1)
				{
					$where .= "id=".$channel_id;
				}else
				{
					$where .= "id=".$channel_id." OR ";
				}

				$index_num = $index + 1;
				$percent_load = floor(($index_num / sizeof($channel_id_arr)) * 100);
				echo "<script>$('.progress').hide();</script>";
				echo '<div class="progress">';
				echo '<div class="progress-bar" role="progressbar" id="loading-bar-bulk" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: '.$percent_load.'%" >'.$percent_load.'%</div>';
				//echo '<div class="progress">';
				echo '</div>';
				usleep(25000);
			}

			$data = array(
				'is_active' => false, 
			);

			$this->projectItem_m->updateItem($data,$where,true);

		}else if($method == 'update')
		{
			//$items = $this->projectItem_m->getItem(array('projectId' => $projectId ));
			foreach ($channel_id_arr as $index => $channel_id) {
				$item = $this->projectItem_m->getItem(array('id' => $channel_id ));
				$channel_data_info = $this->channel_m->getChannel(array('id' => $item[0]->channelId));
				$channel_data = send_request('https://www.googleapis.com/youtube/v3/channels?id='.$channel_data_info[0]->yId.'&part=statistics,snippet');

				foreach ($channel_data->items as $item_key => $item_data) {
				    	
				    $update_data = array( 
				    	'description' => property_exists($item_data->snippet, 'description')? $item_data->snippet->description:'', 
				    	'publishedAt' => property_exists($item_data->snippet, 'publishedAt')? $item_data->snippet->publishedAt:'', 
				    	'thumbnails' => property_exists($item_data->snippet, 'thumbnails')? json_encode($item_data->snippet->thumbnails):'', 
				    	'country' => property_exists($item_data->snippet, 'country')? $item_data->snippet->country:'', 
				    	'viewCount' => property_exists($item_data->statistics, 'viewCount')? $item_data->statistics->viewCount:'', 
				    	'commentCount' => property_exists($item_data->statistics, 'commentCount')? $item_data->statistics->commentCount:'', 
				    	'subscriberCount' => property_exists($item_data->statistics, 'subscriberCount')? $item_data->statistics->subscriberCount:'', 
				    	'videoCount' => property_exists($item_data->statistics, 'videoCount')? $item_data->statistics->videoCount:'', 
				    );

				    $this->channel_m->updateChannel($update_data,$channel_data_info[0]->yId);
					    

			    }

			    $videos = $this->video_m->getChannelVideo($channel_data_info[0]->id,'viewCount','asc',30);
			    $this->video_m->deleteChannelVideos($channel_data_info[0]->id);

			    $videos = send_request('https://www.googleapis.com/youtube/v3/search?channelId='.$channel_data_info[0]->yId.'&part=snippet,id&order=date&maxResults=30&type=video');
					
				if(sizeof($videos->items) == 0)
				{
					
				}else
				{

					foreach ($videos->items as $video_index => $video_data) {
						
						$video_info = send_request('https://www.googleapis.com/youtube/v3/videos?id='.$video_data->id->videoId.'&part=statistics,snippet');

						if(!property_exists($video_info, 'items'))
						{
							

						}else
						{


							$insert_data = array(
						    	'yId' => $video_info->items[0]->id,
						    	'title' => property_exists($video_info->items[0]->snippet, 'title')? $video_info->items[0]->snippet->title:'',  
						    	'viewCount' => property_exists($video_info->items[0]->statistics, 'viewCount')? $video_info->items[0]->statistics->viewCount:'', 
						    	'channelId' => $channel_data_info[0]->id, 
						    	'date' => property_exists($video_info->items[0]->snippet, 'publishedAt')? $video_info->items[0]->snippet->publishedAt:'', 
						    );

						    $channel_video_id = $this->video_m->addChannelVideo($insert_data);
						}
					}
				}

				if(empty($channel_data_info[0]->category))
				{
					$category = [];
					$videos = $this->video_m->getChannelVideo($channel_data_info[0]->id,'viewCount','asc',30);
					if(sizeof($videos) != 0)
					{

						foreach ($videos as $index => $video) {
							
							$video_info = send_request('https://www.googleapis.com/youtube/v3/videos?id='.$video->yId.'&part=statistics,snippet');
							array_push($category,$video_info->items[0]->snippet->categoryId);
						}

						$category_str = "";
						$ccounter= 0;
						$categoryNoDuplicate = array_unique($category);
						$count_index_arr = array();
						foreach ($categoryNoDuplicate as $cindex => $cat) {
							$count_dup = 0;
							foreach ($category as $dupindex => $dup) {
								if($cat == $dup)
								{
									$count_dup++;
								}
							}

							array_push($count_index_arr,[$cindex,$count_dup]);
							
						}

						$category_index = 0;
						$category_count = 0;
						foreach ($count_index_arr as $index => $cat) {
							if($category_count < $cat[1])
							{
								$category_count = $cat[1];
								$category_index = $cat[0];
							}
						}

						$cat_info = send_request('https://www.googleapis.com/youtube/v3/videoCategories?id='.$categoryNoDuplicate[$category_index].'&part=snippet');
						if(!is_null($cat_info->items[0]->snippet->title))
						{
							$category_data = $this->category_m->getCategory(array('name' => $cat_info->items[0]->snippet->title));
							
							if(sizeof($category_data) == 0)
							{
								$data = array(
									'name' => $cat_info->items[0]->snippet->title, 
								);
								$category_id = $this->category_m->addCategory($data);
							}else
							{
								$category_id = $category_data[0]->id;
							}

				
							$update_data = array( 
						    	'categoryId' => $category_id, 
						    );

						    $this->channel_m->updateChannel($update_data,$channel_data_info[0]->yId);
						}
					    
					    
					}
					
				}

				$videos_list_top_20 = $this->video_m->getChannelVideo($channel_data_info[0]->id,'viewCount','asc',20);
				
				if(sizeof($videos_list_top_20) != 0)
				{
					$ave_view = 0;

					foreach ($videos_list_top_20 as $video_index => $data) {
						$ave_view += $data->viewCount;
					}

					$ave_view = floor($ave_view / sizeof($videos_list_top_20));

					$update_data = array( 
				    	'ave_view' => $ave_view, 
				    );

				    $this->channel_m->updateChannel($update_data,$channel_data_info[0]->yId);

				}

				$index_num = $index + 1;
				$percent_load = floor(($index_num / sizeof($channel_id_arr)) * 100);
				echo "<script>$('.progress').hide();</script>";
				echo '<div class="progress">';
				echo '<div class="progress-bar" role="progressbar" id="loading-bar-bulk" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: '.$percent_load.'%" >'.$percent_load.'%</div>';
				//echo '<div class="progress">';
				echo '</div>';

			}


		}

		//$project_details = $this->getProjectItems($projectId);
		
		//echo json_encode($project_details);
	}

	public function getShareLink()
	{
		$this->output->unset_template();

		$project = $this->project_m->getProject(array('id' => $_POST['projectId']));

		if($project[0]->share_token == "")
		{
			$share_token = sharedToken(8);
			$data = array(
				'share_token' => $share_token, 
			);

			$project[0]->share_token = $share_token;

			$this->project_m->updateProject($data,$_POST['projectId']);

		}

		echo base_url().'share/project/'.$project[0]->share_token;

	}

	/**
	 * Get Project Content Version 2
	 */
	public function getProjectItemsv2($passId = null,$forLocal = false)
	{
		$this->output->unset_template();
		$item_response = [];
		$channel_id_list = [];

		if(is_null($passId))
			$projectId = $_POST['id'];
		else
			$projectId = $passId;

		$project = $this->project_m->getProject(array('id' => $projectId));

		$items = $this->projectItem_m->getItem(array('projectId' => $projectId));

		$combine_ave = 0;
		$combine_cost = 0;
		foreach ($items as $index => $i) {
			$channel = $this->channel_m->getChannel(array('id' => $i->channelId ));

			if( sizeof($channel) != 0 ){
				$videos = $this->video_m->getChannelVideo($channel[0]->id,'viewCount','asc',20);
				$video_total_views_average = 0;
				if(sizeof($videos) != 0)
				{
					foreach ($videos as $video_index => $data) {
						$video_total_views_average += $data->viewCount;
					}

					$video_total_views_average = floor($video_total_views_average / sizeof($videos));
				}

				$item_data = array(
					'channelId' => $channel[0]->id,
					'title' => $channel[0]->title,
					'img' => json_decode($channel[0]->thumbnails)->default->url,
					'video' =>  $channel[0]->videoCount,
					'id' => $i->id,
					'email' => $channel[0]->email,
					'ave_view' => $video_total_views_average,
					'cost' => $channel[0]->cost,
					'is_active' => $i->is_active,
				);
				$combine_cost += $i->is_active? $channel[0]->cost:0;
				$combine_ave += $i->is_active? $video_total_views_average:0;

				array_push($item_response, $item_data);
				// array_push($channel_id_list, $channel[0]->id);
			}
		}

		$project[0]->combine_ave = $combine_ave;
		$project[0]->combine_cost = $combine_cost;
		$project[0]->estemated_sales = $combine_ave * $project[0]->cost_rate;
		$project[0]->estemated_gross = $project[0]->estemated_sales * $project[0]->sale_profit;
		$project[0]->estemated_net = $project[0]->estemated_gross - $combine_cost;


		$response = array('items' => $item_response, 'project' => $project[0] );
		// $response = array('items' => $channel_id_list, 'project' => $project[0] );

		if(is_null($passId))
			echo json_encode($response);
			// echo json_encode($items);
		else
			return $project[0];


	}

	
}

?>