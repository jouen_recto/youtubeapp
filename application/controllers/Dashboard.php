<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		set_auth('login');

		$this->load->helper('url');
		$this->load->model('channel_m');
		$this->load->model('video_m');
		$this->load->model('category_m');
		$this->load->model('favorite_m');
		$this->load->model('note_m');
		$this->load->model('insta_m');

		//init pages
		$this->data['page_title'] = 'dashboard';
		$this->data['flash_data'] = $this->session->flashdata();
		
		$this->output->set_template('default');

		
	}

	public function index()
	{
		// $channel_data = $this->channel_m->getAllChannels();
		// $get_data_status = false;
		// if(sizeof($channel_data) == 0)
		// {
		// 	$get_data_status = true;
		// }
		
		// foreach ($channel_data as $index => $channel) {
		// 	$videos = $this->video_m->getChannelVideo($channel->id,'viewCount','asc',20);
		// 	$video_total_views_average = 0;
		// 	if(sizeof($videos) != 0)
		// 	{
		// 		foreach ($videos as $video_index => $data) {
		// 			$video_total_views_average += $data->viewCount;
		// 		}

		// 		$video_total_views_average = $video_total_views_average / sizeof($videos);
		// 	}

		// 	$channel_data[$index]->aveViews = $video_total_views_average;
		// }
		

	    //$this->data['get_data_status'] = $get_data_status;
	    //$this->data['channel_data'] = $channel_data;
	    $this->data['categories'] = $this->category_m->getAllCategories();

	  

	    //load js
		$this->load->js('assets/js/dashboard/dashboard.js');

		$this->load->view('dashboard/index',$this->data);
	}


	public function get_channels()
	{
		$this->output->unset_template();

		$where = null;
		$table_data = [];
		if(isset($_GET['table_data']))
		{	
			$table_data = $_GET['table_data'];
		}


	
		if(isset($_GET['category']))
		{
			$where = array(
				'categoryId' =>  $_GET['category'], 
			);
		}

		if(isset($_GET['favorite']))
		{
			if($_GET['sSearch'] == "")
			{
				$where = "";
				$user = $this->session->userdata('user_data')[0];
				$favorite = $this->favorite_m->getFavorite(array('userId' => $user->id, 'status' => true));

				foreach ($favorite as $index => $fav) {

					if($index != (sizeof($favorite)-1))
						$where.="id=".$fav->channelId." OR ";
					else
						$where.="id=".$fav->channelId;
				}
			}

		}

		if($_GET['table_data'][$_GET['iSortCol_0']] != "aveViews")
		{
			$order_by = array(
				'key' => $_GET['table_data'][$_GET['iSortCol_0']],
				'type' =>  $_GET['sSortDir_0']
			);
		}else
		{
			$order_by = null;
		}

		//if($_GET['category'] == 'default')
		if($_GET['sSearch'] == ""){
			$channel_data = $this->channel_m->getAllChannels($where,$order_by);
		}
		else{
			

			$channel_data = $this->channel_m->searchChannels($_GET['sSearch'],$where,$order_by);

			if(isset($_GET['favorite']))
			{
				$channel_favorite_search = [];
				$user = $this->session->userdata('user_data')[0];
				$favorite = $this->favorite_m->getFavorite(array('userId' => $user->id, 'status' => true));

				foreach ($favorite as $index => $fav) {
					foreach ($channel_data as $index_c => $c) {
						if($fav->channelId == $c->id)
						{
							array_push($channel_favorite_search, $channel_data[$index_c]);
						}
					}
				}

				$channel_data = $channel_favorite_search;
			}

		}


		
		$data_to_return = [];
		$start_data = $_GET['iDisplayStart'];
		for ($i = 0; $i < $_GET['iDisplayLength']; $i++) { 
			if($start_data < sizeof($channel_data))
			{
				// $videos = $this->video_m->getChannelVideo($channel_data[$start_data]->id,'viewCount','asc',20);
				// $video_total_views_average = 0;
				// if(sizeof($videos) != 0)
				// {
				// 	foreach ($videos as $video_index => $data) {
				// 		$video_total_views_average += $data->viewCount;
				// 	}

				// 	$video_total_views_average = $video_total_views_average / sizeof($videos);
				// }

				
				// $channel_data[$start_data]->title = utf8_encode($channel_data[$start_data]->title);
				// $channel_data[$start_data]->description = utf8_encode($channel_data[$start_data]->description);
				
				// $channel_data[$start_data]->aveViews = floor($video_total_views_average);
				// if($start_datandex == 3999)
				array_push($data_to_return, $channel_data[$start_data]);
			}
			$start_data++;
		}
		// foreach ($channel_data as $index => $channel) {
		// 	$videos = $this->video_m->getChannelVideo($channel->id,'viewCount','asc',20);
		// 	$video_total_views_average = 0;
		// 	if(sizeof($videos) != 0)
		// 	{
		// 		foreach ($videos as $video_index => $data) {
		// 			$video_total_views_average += $data->viewCount;
		// 		}

		// 		$video_total_views_average = $video_total_views_average / sizeof($videos);
		// 	}

		// 	$channel_data[$index]->aveViews = $video_total_views_average;
		// 	// if($index == 3999)
		// 	array_push($data_to_return, $channel_data[$index]);
		// 	break;
		// }

		if($_GET['table_data'][$_GET['iSortCol_0']] == "aveViews")
		{
			usort($data_to_return, function($a, $b) {
				if($_GET['sSortDir_0'] == 'asc')
	            	return $a->ave_view - $b->ave_view;
	            else
	            	return $b->ave_view - $a->ave_view;
	        });
		}

		
		echo json_encode(array('data' => $data_to_return));
	}

	public function sync()
	{
		//load js
		$this->load->js('assets/js/dashboard/sync.js');

		$this->load->view('dashboard/sync-index',$this->data);
	}

	public function bench_video()
	{
		$video_data = send_request('https://www.googleapis.com/youtube/v3/search?channelId=UC-lHJZR3Gqxm24_Vd_AJ5Yw&part=snippet,id&order=date&maxResults=30');
		
		echo "<pre>";
		var_dump($video_data);
		echo "</pre>";
	}

	public function view($channelId)
	{
		//user data
		$user_data = $this->session->userdata('user_data')[0];

		//get the data of the channel
		$channel_data_info = $this->channel_m->getChannel(array('id' => $channelId));

		// get the videos of the channel
		//$this->video_m->deleteDuplicate($channelId);
		$videos = $this->video_m->getChannelVideo($channelId,'viewCount','asc',30);

		//get insta
		$insta = $this->insta_m->getInsta(array('channelId'=> $channelId));
		
		//add cateogry name
		$category_data = $this->category_m->getCategory(array('id' => $channel_data_info[0]->categoryId));

		if(sizeof($category_data) != 0)
			$channel_data_info[0]->category = $category_data[0]->name;
		else
			$channel_data_info[0]->category = "";

		$top10 = [];
		$top20 = [];

		for ($i=sizeof($videos); $i > 0 ; $i--) { 
			if(sizeof($top10) != 10)
			{
				array_push($top10,$videos[$i-1]);
			}else
			{
				array_push($top20,$videos[$i-1]);
			}
		}
		
		// $videos_list_top_20 = $this->video_m->getChannelVideo($channelId,'viewCount','asc',20);
		// $video_total_views_average = 0;
		// if(sizeof($videos_list_top_20) != 0)
		// {
		// 	foreach ($videos_list_top_20 as $video_index => $data) {
		// 		$video_total_views_average += $data->viewCount;
		// 	}

		// 	$video_total_views_average = floor($video_total_views_average / sizeof($videos_list_top_20));
		// }

		//check if this channel is favorite by user
		$favorite_by_user = $this->favorite_m->getFavorite(array('userId' => $user_data->id,'channelId' => $channelId));
		$is_favorite = sizeof($favorite_by_user) == 0? false:$favorite_by_user[0]->status;

		//get user notes
		$note = $this->note_m->getNote(array('userId' => $user_data->id,'channelId' => $channelId));

		$this->data['top10'] = $top10;
		$this->data['top20'] = $top20;
		$this->data['video_total_views_average'] = $channel_data_info[0]->ave_view;
		$this->data['channel'] = $channel_data_info[0];
		$this->data['is_favorite'] = $is_favorite;
		$this->data['user_data'] = $user_data;
		$this->data['note'] = $note;
		$this->data['insta'] = sizeof($insta)==0? false:$insta[0];

		//load js
		$this->load->js('assets/js/dashboard/view.js');

		$this->load->view('channel/view',$this->data);
	}
}

?>