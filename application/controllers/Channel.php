<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Channel extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		set_auth('login');
		$this->load->helper('url');
		$this->load->model('channel_m');
		$this->load->model('video_m');
		$this->load->model('category_m');
		$this->load->model('favorite_m');
		$this->load->model('projectItem_m');
		$this->load->model('project_m');
		$this->load->model('insta_m');

		$this->data['page_title'] = 'channel';
		$this->output->set_template('default');
	}

	public function get_data_top_5000()
	{
		$this->output->unset_template();
		// $data = data_scraping(base_url().'data-top-5000.html');
		// echo $data;
		$channel_json = "";
		$channels = $this->channel_m->getAllChannels();

		foreach ($channels as $index => $channel) {
			if($index == 0)
			{
				$channel_json.= "<table><tr class='channel-data' ><td class='channel-id' >".$channel->yId."</td><td class='channel-name' >".$channel->title."</td></tr>";
			}else if($index == (sizeof($channels)-1))
			{
				$channel_json.= "<tr class='channel-data' ><td class='channel-id' >".$channel->yId."</td><td class='channel-name' >".$channel->title."</td></tr></table>";
			}else
			{
				$channel_json.= "<tr class='channel-data' ><td class='channel-id' >".$channel->yId."</td><td class='channel-name' >".$channel->title."</td></tr>";
			}

			
		}
		
		echo $channel_json;
	}

	public function save_channel_data()
	{
		$this->output->unset_template();
		
		$user_data = $this->session->userdata('user_data')[0];

		// $channels = json_decode($_POST['channels']);

		// $value = array(
		// 	'channels' => $channels,
		// );

		$data = array(
			'type' => 'save channel data',
			// 'value' => json_encode($value),
			'value' => '',
			'is_process' => false,
			'userId' => $user_data->id
		);

		$cron_id = $this->cron_m->addCronJob($data);

		cron('save_channel_data',$cron_id);

		$flash_data = array(
			'status' =>  true,
			'msg' => 'All channel in database is now updating' 
		);

		$this->session->set_flashdata($flash_data);

		redirect('dashboard/sync');
		
	}
	public function search()
	{

		
		if(isset($_POST['search-channel']))
		{

			$filter_channel = [];
			$channels = $this->channel_m->searchFrom($_POST);	
			// echo "<pre>";
			// var_dump($channels);
			// echo "</pre>";
			foreach ($channels as $index => $channel) {

				// $ave_view = 0;
				// $videos_list_top_20 = $this->video_m->getChannelVideo($channel->id,'viewCount','asc',20);
				
				// if(sizeof($videos_list_top_20) != 0)
				// {
				// 	foreach ($videos_list_top_20 as $video_index => $data) {
				// 		$ave_view += $data->viewCount;
				// 	}

				// 	$ave_view = floor($ave_view / sizeof($videos_list_top_20));
				// }
				$pass_to_search = true;
				// if((int) $_POST['channel-view-max'] > 0)
				// {
				// 	if($ave_view > $_POST['channel-view-max'])
				// 	{
				// 		$pass_to_search = false;
				// 	}
				// }

				// if((int) $_POST['channel-view-min'] > 0)
				// {
				// 	if($ave_view < $_POST['channel-view-min'])
				// 	{
				// 		$pass_to_search = false;
				// 	}
				// }

				// $channels[$index]->ave_view = $ave_view;

				$insta = $this->insta_m->getInsta(array('channelId' => $channels[$index]->id ));

				$channels[$index]->insta = sizeof($insta) == 0? false:$insta[0];

				if($pass_to_search)
				{
					array_push($filter_channel, $channels[$index]);
				}


			}

			$categories = $this->category_m->getAllCategories();

			$countries = $this->channel_m->getChannelCountries();

			foreach ($countries as $index => $contry) {
				$countries[$index]->country = getCountryName($countries[$index]->country);
			}
			
			$this->data['data_form'] = $_POST;

			$this->data['categories'] = $categories;
			$this->data['countries'] = $countries;
			$this->data['channels'] = $filter_channel;
			$this->data['channels_size'] = sizeof($filter_channel);
			$this->data['search_status'] = true;

			$this->load->js('assets/js/channel/search.js');
			$this->load->css('assets/css/channel/search.css');

			$this->load->view('channel/search',$this->data);
		}else
		{

			$categories = $this->category_m->getAllCategories();

			$countries = $this->channel_m->getChannelCountries();

			foreach ($countries as $index => $contry) {
				$countries[$index]->country = getCountryName($countries[$index]->country);
			}
			

			
			$this->load->js('assets/js/channel/search.js');
			$this->load->css('assets/css/channel/search.css');

			$this->data['search_status'] = false;
			$this->data['categories'] = $categories;
			$this->data['countries'] = $countries;

			$this->load->view('channel/search',$this->data);
		}
	}

	public function autocomplete()
	{
		$this->output->unset_template();

		$channels = $this->channel_m->searchChannels($_POST['q']);

		$result = [];



		if(sizeof($channels) != 0)
		{
			foreach ($channels as $index => $channel_data) {

				array_push($result,array('label' => $channel_data->title,'value' => $channel_data->yId ));
			}
		}

		echo json_encode($result);
	}

	public function getChannelData()
	{
		$this->output->unset_template();

		$id = $_POST['channelId'];

		$user_data = $this->session->userdata('user_data')[0];

		$channel_data = send_request('https://www.googleapis.com/youtube/v3/channels?id='.$id.'&part=statistics,snippet');

		
		$result = false;

		if(!empty($channel_data->items))
		{
			$channel_db_id = 0;
			$already_save = false;
			$is_favorite = false;
			$channel_data_info = $this->channel_m->getChannel(array('yId' => $channel_data->items[0]->id));
			$channel_url = false;
			if(!empty($channel_data_info)){
				$already_save = true;
				$channel_db_id = $channel_data_info[0]->id;

				$favorite  = $this->favorite_m->getFavorite(array('userId' => $user_data->id,'channelId' => $channel_db_id));
				if(sizeof($favorite) != 0)
				{
					if($favorite[0]->status)
						$is_favorite = true;
				}

				$channel_url = base_url().'dashboard/view/'.$channel_db_id;
			}

			$ave_view = 0;
			
		
			if($already_save)
			{
				// $videos_list_top_20 = $this->video_m->getChannelVideo($channel_db_id,'viewCount','asc',20);
				
				// if(sizeof($videos_list_top_20) != 0)
				// {
				// 	foreach ($videos_list_top_20 as $video_index => $data) {
				// 		$ave_view += $data->viewCount;
				// 	}

					
					$ave_view = $channel_data_info[0]->ave_view;
				// }
			}else{

				$videos = send_request('https://www.googleapis.com/youtube/v3/search?channelId='.$channel_data->items[0]->id.'&part=snippet,id&order=date&maxResults=30&type=video');

				if(sizeof($videos->items) != 0)
				{

					foreach ($videos->items as $video_index => $video_data) {
						
						$video_info = send_request('https://www.googleapis.com/youtube/v3/videos?id='.$video_data->id->videoId.'&part=statistics,snippet');
						$ave_view += $video_info->items[0]->statistics->viewCount;
						
					}

					$ave_view = $ave_view/sizeof($videos->items);
				}
			}
			$result = array(
				'title' => $channel_data->items[0]->snippet->title, 
				'img' => $channel_data->items[0]->snippet->thumbnails->default->url, 
				'subscriber' => $channel_data->items[0]->statistics->subscriberCount,
				'view' => $channel_data->items[0]->statistics->viewCount,
				'video' => $channel_data->items[0]->statistics->videoCount,
				'id' => $channel_data->items[0]->id,
				'ave_view' => $ave_view,
				'url' => $channel_url,
				'is_save' => $already_save,
				'user_id' => $user_data->id,
				'channel_id' => $channel_db_id,
				'is_favorite' => $is_favorite,
			);
			
		}


		echo json_encode($result);
		
	}

	public function addChannel($channelId = null,$csv = false)
	{
		$this->output->unset_template();

		if(is_null($channelId))
			$id = $_POST['channelId'];
		else
			$id = $channelId;

		$channel_data = send_request('https://www.googleapis.com/youtube/v3/channels?id='.$id.'&part=statistics,snippet');


		foreach ($channel_data->items as $item_key => $item_data) {
							    	
		    $insert_data = array(
		    	'yId' => $item_data->id,
		    	'title' => property_exists($item_data->snippet, 'title')? $item_data->snippet->title:'', 
		    	'description' => property_exists($item_data->snippet, 'description')? $item_data->snippet->description:'', 
		    	'publishedAt' => property_exists($item_data->snippet, 'publishedAt')? $item_data->snippet->publishedAt:'', 
		    	'thumbnails' => property_exists($item_data->snippet, 'thumbnails')? json_encode($item_data->snippet->thumbnails):'',
		    	'country' => property_exists($item_data->snippet, 'country')? $item_data->snippet->country:'', 
		    	'viewCount' => property_exists($item_data->statistics, 'viewCount')? $item_data->statistics->viewCount:'', 
		    	'commentCount' => property_exists($item_data->statistics, 'commentCount')? $item_data->statistics->commentCount:'', 
		    	'subscriberCount' => property_exists($item_data->statistics, 'subscriberCount')? $item_data->statistics->subscriberCount:'', 
		    	'videoCount' => property_exists($item_data->statistics, 'videoCount')? $item_data->statistics->videoCount:'', 
		    );

		    $channel_id = $this->channel_m->addChannel($insert_data);


		    $videos = send_request('https://www.googleapis.com/youtube/v3/search?channelId='.$id.'&part=snippet,id&order=date&maxResults=30&type=video');

		    foreach ($videos->items as $video_index => $video_data) {
							
				$video_info = send_request('https://www.googleapis.com/youtube/v3/videos?id='.$video_data->id->videoId.'&part=statistics,snippet');

				if(!property_exists($video_info, 'items'))
				{
					

				}else
				{


					$insert_data = array(
				    	'yId' => $video_data->id->videoId,
				    	'title' => property_exists($video_info->items[0]->snippet, 'title')? $video_info->items[0]->snippet->title:'',  
				    	'viewCount' => property_exists($video_info->items[0]->statistics, 'viewCount')? $video_info->items[0]->statistics->viewCount:'', 
				    	'channelId' => $channel_id, 
				    	'date' => property_exists($video_info->items[0]->snippet, 'publishedAt')? $video_info->items[0]->snippet->publishedAt:'', 
				    );

				    $videos_id = $this->video_m->addChannelVideo($insert_data);
				   
				    // sleep(2);
				}
			}
			 

			$category = [];
			$videos = $this->video_m->getChannelVideo($channel_id,'viewCount','asc',30);

			if(sizeof($videos) != 0)
			{

				foreach ($videos as $index => $video) {
					
					$video_info = send_request('https://www.googleapis.com/youtube/v3/videos?id='.$video->yId.'&part=statistics,snippet');
					
					array_push($category,$video_info->items[0]->snippet->categoryId);
				}

				$category_str = "";
				$ccounter= 0;
				$categoryNoDuplicate = array_unique($category);
				$count_index_arr = array();
				foreach ($categoryNoDuplicate as $cindex => $cat) {
					$count_dup = 0;
					foreach ($category as $dupindex => $dup) {
						if($cat == $dup)
						{
							$count_dup++;
						}
					}

					array_push($count_index_arr,[$cindex,$count_dup]);
					
				}

				$category_index = 0;
				$category_count = 0;
				foreach ($count_index_arr as $index => $cat) {
					if($category_count < $cat[1])
					{
						$category_count = $cat[1];
						$category_index = $cat[0];
					}
				}

				$cat_info = send_request('https://www.googleapis.com/youtube/v3/videoCategories?id='.$categoryNoDuplicate[$category_index].'&part=snippet');

				if(!is_null($cat_info->items[0]->snippet->title))
				{
					$category_data = $this->category_m->getCategory(array('name' => $cat_info->items[0]->snippet->title));
							
					if(sizeof($category_data) == 0)
					{
						$data = array(
							'name' => $cat_info->items[0]->snippet->title, 
						);
						$category_id = $this->category_m->addCategory($data);
					}else
					{
						$category_id = $category_data[0]->id;
					}

		
					$update_data = array( 
				    	'categoryId' => $category_id, 
				    );

				    $this->channel_m->updateChannel($update_data,$id);
				}
			    
			}  

			$videos_list_top_20 = $this->video_m->getChannelVideo($channel_id,'viewCount','asc',20);
					
			if(sizeof($videos_list_top_20) != 0)
			{
				$channel_yid = $this->channel_m->getChannel(array('id' => $channel_id))[0]->yId;
				$ave_view = 0;
				
				foreach ($videos_list_top_20 as $video_index => $data) {
					$ave_view += $data->viewCount;
				}

				$ave_view = floor($ave_view / sizeof($videos_list_top_20));

		
				$update_data = array( 
			    	'ave_view' => $ave_view, 
			    );
				
			    $this->channel_m->updateChannel($update_data,$channel_yid);
			   

			} 


	    }


	   	$response = array('status' => true,'message' => 'Channel Successfully Added.', 'channelId' => $channel_id );

	   	if(is_null($channelId))
	   		echo json_encode($response);
	   	else if($csv)
	   		return $channel_id;
	}


	public function import_bulk_channel()
	{
		$this->output->unset_template();
		$user_data = $this->session->userdata('user_data')[0];
		$bulk_channels = json_decode($_POST['bulk-channel-value']);

		$value = array(
			'bulk_channels' => $bulk_channels,
			'project_id' => $_POST['project-id'],
		);

		$data = array(
			'type' => 'import channel url',
			'value' => json_encode($value),
			'is_process' => false,
			'userId' => $user_data->id
		);

		$cron_id = $this->cron_m->addCronJob($data);

		cron('import_bulk_channel',$cron_id);

		$flash_data = array(
			'status' =>  true,
			'msg' => 'Your channel is now processing',
			'projectId' => $_POST['project-id']
		);

		$this->session->set_flashdata($flash_data);

		redirect('import/url');
		
	}


	public function saveEmail()
	{
		$this->output->unset_template();
		$id = $_POST['channel_id'];
		$email = $_POST['email'];

		if($_POST['type'] == "first-email")
		{
			$data = array(
				'email' => $email, 
			);
		}else
		{
			$data = array(
				'second_email' => $email, 
			);
		}

		$this->channel_m->updateChannel($data,$id);
	}


	public function saveCost()
	{
		$this->output->unset_template();
		$id = $_POST['channel_id'];
		$cost = $_POST['cost'];

		$data = array(
			'cost' => $cost, 
		);

		$this->channel_m->updateChannel($data,$id);
	}

	public function updateSingleChannel($pass_channel_id = null)
	{

		$this->output->unset_template();
		if(is_null($pass_channel_id))
			$channel_id = $_POST['channel-id'];
		else
			$channel_id = $pass_channel_id;

		$channel = $this->channel_m->getChannel(array('id' => $channel_id));

		$channel_data = send_request('https://www.googleapis.com/youtube/v3/channels?id='.$channel[0]->yId.'&part=statistics,snippet');

		foreach ($channel_data->items as $item_key => $item_data) {
							    	
		    $insert_data = array(
		    	'yId' => $item_data->id,
		    	'title' => property_exists($item_data->snippet, 'title')? $item_data->snippet->title:'', 
		    	'description' => property_exists($item_data->snippet, 'description')? $item_data->snippet->description:'', 
		    	'publishedAt' => property_exists($item_data->snippet, 'publishedAt')? $item_data->snippet->publishedAt:'', 
		    	'thumbnails' => property_exists($item_data->snippet, 'thumbnails')? json_encode($item_data->snippet->thumbnails):'',
		    	'country' => property_exists($item_data->snippet, 'country')? $item_data->snippet->country:'', 
		    	'viewCount' => property_exists($item_data->statistics, 'viewCount')? $item_data->statistics->viewCount:'', 
		    	'commentCount' => property_exists($item_data->statistics, 'commentCount')? $item_data->statistics->commentCount:'', 
		    	'subscriberCount' => property_exists($item_data->statistics, 'subscriberCount')? $item_data->statistics->subscriberCount:'', 
		    	'videoCount' => property_exists($item_data->statistics, 'videoCount')? $item_data->statistics->videoCount:'', 
		    );

		    $this->channel_m->updateChannel($insert_data,$channel_id);

		    $this->video_m->deleteChannelVideos($channel_id);

		    $videos = send_request('https://www.googleapis.com/youtube/v3/search?channelId='.$channel[0]->yId.'&part=snippet,id&order=date&maxResults=30&type=video');


		    foreach ($videos->items as $video_index => $video_data) {
							
				$video_info = send_request('https://www.googleapis.com/youtube/v3/videos?id='.$video_data->id->videoId.'&part=statistics,snippet');

				if(!property_exists($video_info, 'items'))
				{
					

				}else
				{


					$insert_data = array(
				    	'yId' => $video_data->id->videoId,
				    	'title' => property_exists($video_info->items[0]->snippet, 'title')? $video_info->items[0]->snippet->title:'',  
				    	'viewCount' => property_exists($video_info->items[0]->statistics, 'viewCount')? $video_info->items[0]->statistics->viewCount:'', 
				    	'channelId' => $channel_id, 
				    	'date' => property_exists($video_info->items[0]->snippet, 'publishedAt')? $video_info->items[0]->snippet->publishedAt:'', 
				    );

				    $videos_id = $this->video_m->addChannelVideo($insert_data);
				   
				    // sleep(2);
				}
			}
			 

			$category = [];
			$videos = $this->video_m->getChannelVideo($channel_id,'viewCount','asc',30);

			if(sizeof($videos) != 0)
			{

				foreach ($videos as $index => $video) {
					
					$video_info = send_request('https://www.googleapis.com/youtube/v3/videos?id='.$video->yId.'&part=statistics,snippet');
					
					array_push($category,$video_info->items[0]->snippet->categoryId);
				}

				$category_str = "";
				$ccounter= 0;
				$categoryNoDuplicate = array_unique($category);
				$count_index_arr = array();
				foreach ($categoryNoDuplicate as $cindex => $cat) {
					$count_dup = 0;
					foreach ($category as $dupindex => $dup) {
						if($cat == $dup)
						{
							$count_dup++;
						}
					}

					array_push($count_index_arr,[$cindex,$count_dup]);
					
				}

				$category_index = 0;
				$category_count = 0;
				foreach ($count_index_arr as $index => $cat) {
					if($category_count < $cat[1])
					{
						$category_count = $cat[1];
						$category_index = $cat[0];
					}
				}

				$cat_info = send_request('https://www.googleapis.com/youtube/v3/videoCategories?id='.$categoryNoDuplicate[$category_index].'&part=snippet');

				if(!is_null($cat_info->items[0]->snippet->title))
				{
					$category_data = $this->category_m->getCategory(array('name' => $cat_info->items[0]->snippet->title));
							
					if(sizeof($category_data) == 0)
					{
						$data = array(
							'name' => $cat_info->items[0]->snippet->title, 
						);
						$category_id = $this->category_m->addCategory($data);
					}else
					{
						$category_id = $category_data[0]->id;
					}

		
					$update_data = array( 
				    	'categoryId' => $category_id, 
				    );

				    $this->channel_m->updateChannel($update_data,$channel_id);
				}
			    
			}  

			$videos_list_top_20 = $this->video_m->getChannelVideo($channel_id,'viewCount','asc',20);
					
			if(sizeof($videos_list_top_20) != 0)
			{
				$channel_yid = $this->channel_m->getChannel(array('id' => $channel_id))[0]->yId;
				$ave_view = 0;
				
				foreach ($videos_list_top_20 as $video_index => $data) {
					$ave_view += $data->viewCount;
				}

				$ave_view = floor($ave_view / sizeof($videos_list_top_20));

		
				$update_data = array( 
			    	'ave_view' => $ave_view, 
			    );
				
			    $this->channel_m->updateChannel($update_data,$channel_yid);
			   

			}  
	    }

	    if(is_null($pass_channel_id))
			redirect('dashboard/view/'.$channel[0]->id);
	}


	public function csvFile()
	{
		$this->output->unset_template();
		
		$user_data = $this->session->userdata('user_data')[0];

		//get items in csv
		$csv=str_getcsv(file_get_contents($_FILES["csv-file"]["tmp_name"]));


		$temp_item = [];
		$items = [];
		foreach ($csv as $index => $val) {
			
			if(strpos($val, "\n") !== FALSE) {
			 	$enter_str = explode(PHP_EOL, $val);
			 	
			 	array_push($temp_item,$enter_str[0]);
			 	array_push($items, $temp_item);
			 	$temp_item = [];

			 	array_push($temp_item,$enter_str[1]);

			}else
			{
				array_push($temp_item,$val);
			}
		}

		$value = array(
			'items' => $items,
		);

		$data = array(
			'type' => 'import channel csv',
			'value' => json_encode($value),
			'is_process' => false,
			'userId' => $user_data->id
		);

		$cron_id = $this->cron_m->addCronJob($data);

		cron('csvFile',$cron_id);

		$flash_data = array(
			'status' =>  true,
			'msg' => 'Your channel is now processing' 
		);

		$this->session->set_flashdata($flash_data);

		redirect('import/csv');

		
	}

	public function archiveChannel()
	{
		$this->output->unset_template();
		$itemId = $_POST['itemId'];

		$channel = $this->channel_m->getChannel(array('id' => $itemId));

		$data = array(
			'is_archive' => true, 
		);

		$this->channel_m->updateChannel($data,$channel[0]->yId);

		$response = array('status' => true );
		echo json_encode($response);
	}

	public function refreshProjectChannel()
	{
		$this->output->unset_template();
		$projectId = $_POST['projectId'];
			
		$project_items = $this->projectItem_m->getItem(array('projectId' => $projectId));

		foreach ($project_items as $item_index => $item) {
			$this->updateSingleChannel($item->channelId);
		}	

		$response = array('status' => true );

		echo json_encode($response);
	}

	public function loadChannelresult()
	{
		$this->output->unset_template();
		

		$filter_channel = [];
		$channels = $this->channel_m->searchFrom($_POST);
		

		foreach ($channels as $index => $channel) {

			$channels[$index]->country = getCountryName($channels[$index]->country);
			
			//$channels[$index]->description = $short_desc.'...';

			$insta = $this->insta_m->getInsta(array('channelId' => $channels[$index]->id ));
			$channels[$index]->insta = sizeof($insta) == 0? false:$insta[0];

			
			array_push($filter_channel, $channels[$index]);
			


		}

		$response = array(
			'channel' => $filter_channel,
			'size' => sizeof($filter_channel)+$_POST['search-offset'],
		);

		echo json_encode($response);
	}

	public function searchVideo()
	{

		if(isset($_POST['search-video']))
		{

			$filter_video = [];
			$videos = $this->video_m->searchFrom($_POST);	
			
			foreach ($videos as $index => $channel) {

				$pass_to_search = true;
				

				if($pass_to_search)
				{
					array_push($filter_video, $videos[$index]);
				}


			}


			$this->data['data_form'] = $_POST;

		
			$this->data['videos'] = $filter_video;
			$this->data['videos_size'] = sizeof($filter_video);
			$this->data['search_status'] = true;

			$this->load->js('assets/js/channel/search.js');
			$this->load->css('assets/css/channel/search.css');

			$this->load->view('channel/search_video',$this->data);
		}else
		{

			// $categories = $this->category_m->getAllCategories();

			// $countries = $this->channel_m->getChannelCountries();

			// foreach ($countries as $index => $contry) {
			// 	$countries[$index]->country = getCountryName($countries[$index]->country);
			// }
			

			
			$this->load->js('assets/js/channel/search.js');
			$this->load->css('assets/css/channel/search.css');

			$this->data['search_status'] = false;
			// $this->data['categories'] = $categories;
			// $this->data['countries'] = $countries;

			$this->load->view('channel/search_video',$this->data);
		}
	}

	public function loadVideoresult()
	{
		$this->output->unset_template();
		

		$filter_video = [];
		$videos = $this->video_m->searchFrom($_POST);	
		
		foreach ($videos as $index => $channel) {

			$pass_to_search = true;
			

			if($pass_to_search)
			{
				array_push($filter_video, $videos[$index]);
			}


		}

		$response = array(
			'video' => $filter_video,
			'size' => sizeof($filter_video)+$_POST['search-offset'],
		);

		echo json_encode($response);
	}
}

?>