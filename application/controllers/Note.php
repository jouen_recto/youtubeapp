<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Note extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		set_auth('login');

		$this->load->helper('url');
		$this->load->model('note_m');
		$this->load->model('channel_m');

		//init pages
		$this->data['page_title'] = 'note';
		$this->output->set_template('default');

		
	}

	public function add()
	{
		$this->output->unset_template();

		$id = $_POST['id'];
		$note = $_POST['note'];
		$channel = $_POST['channel'];

		//user data
		$user_data = $this->session->userdata('user_data')[0];

		$data = array(
			'userId' => $user_data->id,
			'channelId' => $channel,
			'note' => $note, 
		);

		$noteId = $this->note_m->addNote($data);

		echo $noteId;

	}

	public function update()
	{
		$this->output->unset_template();

		$id = $_POST['id'];
		$note = $_POST['note'];
		$channel = $_POST['channel'];


		$data = array(
			'note' => $note, 
		);

		$this->note_m->updateNote($data,$id);

		echo $id;

	}

	public function noteList()
	{
		//get user details
		$user_data = $this->session->userdata('user_data')[0];

		$note = $this->note_m->getNote(array('userId'=>$user_data->id));

		foreach ($note as $index => $n) {
			$channel = $this->channel_m->getChannel(array('id' => $n->channelId ));

			$note[$index]->channel_data = $channel[0];
		}

		$this->load->js('assets/js/note/list.js');

		$this->data['note'] = $note;
		$this->load->view('note/list',$this->data);
	}

	public function delete()
	{
		$id = $_POST['id'];

		$this->note_m->deleteNote($id);

		echo true;
	}
}
?>