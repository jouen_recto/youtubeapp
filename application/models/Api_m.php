<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_m extends CI_Model {

    private $table_name;

    public function __construct()
    {
    	parent::__construct();
    	$this->table_name = "api_key";
    }

    

    public function addApiKey($data)
    {
    	$this->db->insert($this->table_name,$data);
    	return $this->db->insert_id();
    }

    public function getApiKey($where)
    {
        $this->db->select('*');
        $this->db->where($where);
        $this->db->from($this->table_name);
        $query = $this->db->get();
        return $query->result();
    }

    public function updateApiKey($where,$data)
    {
        $this->db->where($where);
        // $data = array('api_status' => true );
        $this->db->update($this->table_name, $data);
    }
}
?>