<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product_m extends CI_Model {

    private $table_name;

    public function __construct()
    {
    	parent::__construct();
    	$this->table_name = "products";
    }

    

    public function addProduct($data)
    {
    	$this->db->insert($this->table_name,$data);
    	return $this->db->insert_id();
    }

    public function getProduct($where = null)
    {
        $this->db->select('*');
        
        if(!is_null($where))
            $this->db->where($where);

        $this->db->from($this->table_name);
        $query = $this->db->get();
        return $query->result();
    }

    public function updateProduct($data,$where = null)
    {
       
        $this->db->where($where);
    
        $this->db->update($this->table_name, $data);
    }

    public function deleteProduct($where = null)
    {
        if(!is_null($where))
        {
            $this->db->where($where);
            $this->db->delete($this->table_name);
            return true;
        }else
        {
            return false;
        }
    }
}
?>