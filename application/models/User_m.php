<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_m extends CI_Model {

    private $table_name;

    public function __construct()
    {
    	parent::__construct();
    	$this->table_name = "user";
    }

    

    public function addUser($data)
    {
    	$this->db->insert($this->table_name,$data);
    	return $this->db->insert_id();
    }

    public function getUser($where)
    {
        $this->db->select('*');
        $this->db->where($where);
        $this->db->from($this->table_name);
        $query = $this->db->get();
        return $query->result();
    }

    public function SampleUpdate($id)
    {
        $this->db->where('id', ($id+1));
        $data = array('status' => 'true' );
        $this->db->update($this->table_name, $data);
    }
}
?>