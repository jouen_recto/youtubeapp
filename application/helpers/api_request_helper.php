<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('send_request'))
{
    function send_request($url,$params= null)
	{

		//set api key
		$CI = get_instance();
		$CI->load->model('api_m');
		$where = array('api_status' => true );
		$api = $CI->api_m->getApiKey($where);
		$key = $api[0]->api_key;

		$CI->api_m->updateApiKey(array('api_status' => false ),array('api_status' => true));
		$CI->api_m->updateApiKey(array('id' => $api[0]->id),array('api_status' => false));


	//	$key = "AIzaSyAXvj98E7Z1F1Q7-gGtaV9ywia3A2wNRjY";



	  $postData = '';
	   //create name value pairs seperated by &
	 	if($params != null)
	 	{
	 		foreach($params as $k => $v) 
		   { 
		      $postData .= $k . '='.$v.'&'; 
		   }
		   $postData = rtrim($postData, '&');
		   $postData .='&key='.$key;
	 	}else
	 	{
	 		$url.='&key='.$key;
	 	}
	   
	 
	    $ch = curl_init();  
	 	set_time_limit(0);
	    curl_setopt($ch,CURLOPT_URL,$url);
	    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
	    if($params != null)
	    {
	    	curl_setopt($ch,CURLOPT_HEADER, false); 
		    curl_setopt($ch, CURLOPT_POST, count($postData));
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);  
	    }
	      
	 
	    $output=json_decode(curl_exec($ch));
	 
	    curl_close($ch);

	    return $output;
	 
	}
}
if ( ! function_exists('register_access_token'))
{
	function register_access_token($code)
	{
		$url = "https://api.instagram.com/oauth/access_token";
		$data = array(
			'code' => $code, 
			'client_id' => 'd38df609ed7c49dc89745f8ec170e99b',
			'client_secret' => 'ab69cb88c82a45fd8855d9bf65668a03',
			'grant_type' => 'authorization_code',
			'redirect_uri' => 'http://c3mailer.com/youtubeApp/index.php/bench/data',

		);
		$postData = "";
		foreach($data as $k => $v) 
	   { 
	      $postData .= $k . '='.$v.'&'; 
	   }
		   
		   

	 	$ch = curl_init();  
	 	set_time_limit(0);
	    curl_setopt($ch,CURLOPT_URL,$url);
	    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
	    // if($params != null)
	    // {
	    	curl_setopt($ch,CURLOPT_HEADER, false); 
		    curl_setopt($ch, CURLOPT_POST, count($postData));
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);  
	    //}
	      
	 
	    $output=json_decode(curl_exec($ch));
	 
	    curl_close($ch);

	  	
	  	$_SESSION['access_token'] = $output->access_token;
	}
}


if ( ! function_exists('send_instagram'))
{
    function send_instagram($url,$params= null,$status = true)
	{

	  	$postData = '';
	   //create name value pairs seperated by &
	 	if($params != null)
	 	{
	 		foreach($params as $k => $v) 
		   { 
		      $postData .= $k . '='.$v.'&'; 
		   }
		   $postData = rtrim($postData, '&');
		   $postData .='&access_token='.$_SESSION['access_token'];
	 	}else
	 	{
	 		if($status)
	 			$url.='&access_token='.$_SESSION['access_token'];
	 		else
	 			$url.='?access_token='.$_SESSION['access_token'];
	 	}
	   
	 	
	    $ch = curl_init();  
	 	set_time_limit(0);
	    curl_setopt($ch,CURLOPT_URL,$url);
	    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
	    if($params != null)
	    {
	    	curl_setopt($ch,CURLOPT_HEADER, false); 
		    curl_setopt($ch, CURLOPT_POST, count($postData));
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);  
	    }
	      
	 
	    $output=json_decode(curl_exec($ch));
	 
	    curl_close($ch);

	    return $output;
	 
	}
}

if(! function_exists('data_scraping'))
{
	function data_scraping($url)
	{
		 $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		$html_source = curl_exec($ch);
		curl_close($ch);
	    return $html_source;
	}
}

if(! function_exists('amazon_api'))
{
	function amazom_api($params)
	{
		// Your Access Key ID, as taken from the Your Account page
		$access_key_id = "AKIAJC6CZCPCDS6BUGMA";

		// Your Secret Key corresponding to the above ID, as taken from the Your Account page
		$secret_key = "mh0e3EdRwSkSxa8/xWa6paMTxV4jX8PQiNWneoNB";

		// The region you are interested in
		$endpoint = "webservices.amazon.com";

		$uri = "/onca/xml";

		// Set current timestamp if not set
		if (!isset($params["Timestamp"])) {
		    $params["Timestamp"] = gmdate('Y-m-d\TH:i:s\Z');
		}

		// Sort the parameters by key
		ksort($params);

		$pairs = array();

		foreach ($params as $key => $value) {
		    array_push($pairs, rawurlencode($key)."=".rawurlencode($value));
		}

		// Generate the canonical query
		$canonical_query_string = join("&", $pairs);

		// Generate the string to be signed
		$string_to_sign = "GET\n".$endpoint."\n".$uri."\n".$canonical_query_string;

		// Generate the signature required by the Product Advertising API
		$signature = base64_encode(hash_hmac("sha256", $string_to_sign, $secret_key, true));

		// Generate the signed URL
		$request_url = 'http://'.$endpoint.$uri.'?'.$canonical_query_string.'&Signature='.rawurlencode($signature);

		//echo "Signed URL: \"".$request_url."\"";

		$ch = curl_init();  
	 	set_time_limit(0);
	    curl_setopt($ch,CURLOPT_URL,$request_url);
	    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

     	$output=curl_exec($ch);
	 
	    curl_close($ch);

	    $xml = new SimpleXMLElement($output);
	    
	    return $xml;
	}
}

?>