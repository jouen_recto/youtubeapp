<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



if ( ! function_exists('set_auth'))
{
    function set_auth($type = "login")
    {
        $CI = get_instance();
        $CI->load->helper('url');

    	if($type == "login")
    	{
    		if(!isset($_SESSION['user_data']))
    			header('location: '.base_url().'login');
    	}
    }
}

if(!function_exists('is_login'))
{
	function is_login()
	{
        $CI = get_instance();
        $CI->load->helper('url');

		if(isset($_SESSION['user_data']))
			header('location: '.base_url().'project/projectList');


	}
}
if(!function_exists('sharedToken'))
{
    function sharedToken($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}

?>