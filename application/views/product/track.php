<div class="row" >
	<h3>track Data</h3>
	<div class="col-md-3" >
		<div class="well well-sm">
            <div class="row">
                <div class="col-sm-6 col-md-4">
                    <img src="<?= $item->img ?>" alt="" class="img-rounded img-responsive" />
                </div>
                <div class="col-sm-6 col-md-8">
                    <h4><?= $item->asin; ?></h4>
                    <small><cite title=""><?= $item->title ?></cite></small>
                    <p>
                        <?= $item->sellerNickname ?>
                        <br />
                        <?php if($item->price != 'none'){ ?>
                          <?= json_decode($item->price)->FormattedPrice ?>
                        <?php }else{ ?>
                          <?= $item->price ?>
                        <?php } ?>
                        <br />
                    </p>
                    <!-- Split button -->
                </div>
            </div>
        </div>
	</div>
	<div class="col-md-9" >
		<table class="table table-bordered" style="width: auto;">
			<?php $quantity = $item->quantity; ?>
			<tbody>
				
				<tr>
					<td style="background-color: #34495e;text-align: center;color: #fff;">Date</td>
					<?php $already_arr = array(); ?>
					<?php foreach ($quantity as $quan => $q) { ?>
						<?php if(!in_array($q->createdAt, $already_arr)){ ?>
							<td><?= date('d M Y', $q->createdAt); ?></td>
						<?php } ?>
						<?php array_push($already_arr, $q->createdAt); ?>
					<?php } ?>
				</tr>
				<tr>
					<td style="background-color: #34495e;text-align: center;color: #fff;">Stock</td>
					<?php $already_arr = array(); ?>
					<?php foreach ($quantity as $quan => $q) { ?>
						<?php if(!in_array($q->createdAt, $already_arr)){ ?>
							<td><?= $q->quantity ?></td>
						<?php } ?>
						<?php array_push($already_arr, $q->createdAt); ?>
					<?php } ?>
				</tr>
				<tr>
					<td style="background-color: #34495e;text-align: center;color: #fff;">Diff</td>
					<?php $already_arr = array(); ?>
					<?php foreach ($quantity as $quan => $q) { ?>
						<?php if(!in_array($q->createdAt, $already_arr)){ ?>
							<?php
								if($quan == 0)
								{
									$quan_diff = 0;
									$quan_recent = (int) $q->quantity;
								}else
								{
									$quan_diff = (int) $q->quantity - $quan_recent;

									if($quan_diff != 0)
									{
										if($quan_diff > 0)
										{
											$quan_diff = "+ ".$quan_diff;
										}else
										{
											$quan_diff = $quan_diff * -1;
											$quan_diff = "- ".$quan_diff;
										}
									} 

									$quan_recent = (int) $q->quantity;
								}
							?>
							<td><?= $quan_diff ?></td>
							<?php array_push($already_arr, $q->createdAt); ?>
						<?php } ?>
					<?php } ?>
				</tr>
			</tbody>
		</table>
	</div>
</div>