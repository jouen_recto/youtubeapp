<div class="row" >
	<div class="col-md-12" >
		<?php if(sizeof($flash_data) > 0){ ?>
			<input id='return_projectId' type="hidden" value="<?= $flash_data['projectId']; ?>">
			<div class="alert alert-success">
			  <strong>Success!</strong> <?= $flash_data['msg'] ?>.
			</div>
		<?php } ?>
		<div class="form-group">
		  <label for="comment">Select Project:</label>
		  <select id="sle-project" class="form-control" >
		  	<option value="default" >Import to Database</option>
		  	<?php foreach ($project as $index => $p) { ?>
		  		
		  		<option value="<?= $p->id ?>" ><?= $p->title ?></option>
		  	<?php } ?>
		  </select>
		</div> 
		<div class="form-group">
		  <label for="comment">Put Youtube Channels Url:</label>
		  <textarea class="form-control" rows="5" id="channel-url"></textarea>
		</div> 
        <button class="btn btn-primary" id="btn-import-channel" >import Channels</button><span id="loading-icon" style="display: none;" ><i class="fa fa-circle-o-notch fa-spin" ></i>Adding the channels...</span>
        <form method="POST" action="<?= base_url(); ?>channel/import_bulk_channel" id="form-import-bulk-channel" >
        	<input type="hidden" value="" name="bulk-channel-value" id="bulk-channel-value" >
        	<input type="hidden" value="" name="project-id" id="project-id" >
        </form>
        <!-- <iframe src="" id="load-import-bulk-channel" name="load-import-bulk-channel" style="width:100%;height:400px;border: none;" ></iframe> -->
	</div>
</div>