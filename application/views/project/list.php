<style type="text/css">
  #channel-info-loader {
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      z-index: 999;
      background-color: rgba(14,14,14,0.6);
      right: 0;
      bottom: 0;
      color: #fff;
      font-size: 31px;
  }
  #load-bulk-channel {
      height: 40px;
      border: none;
      overflow: hidden;
      position: fixed;
      z-index: 9999999;
      border-radius: 4px;
      width: 300px;
      top: 70%;
      left: 50%;
      margin-left: -150px;
      margin-top: -20px;
  }
  #load-computation-p {
      height: 40px;
      border: none;
      overflow: hidden;
      position: fixed;
      z-index: 9999999;
      border-radius: 4px;
      width: 300px;
      top: 70%;
      left: 50%;
      margin-left: -150px;
      margin-top: -20px;
      color: #fff;
      text-align: center;
  }
</style>
<iframe src="" id="download-csv-project" style="display: none;" ></iframe>
<iframe src="" id="load-bulk-channel" name="load-bulk-channel" style="display: none;" ></iframe>
<p id="load-computation-p" style="display: none;" >Computing project data...</p>
<div class="row">
  <div class="col-md-12" >
    
    <div class="form-group">
        <label for="sel1">Select Project:</label>
        <select class="form-control" id="sel-list">
          <option value="default" >-- Select Project --</option>
          <?php foreach ($project as $index => $p) { ?>
            <option value="<?= $p->id ?>" ><?= $p->title ?></option>
          <?php } ?>
        </select>
    </div> 
  </div>
</div>
<div class="row" >
  <div id="panel-action-project" style="display: none;" class="col-md-12" >
    <button class="btn btn-default" id="btn-share-project" >
        <i class="fa fa-share-alt" aria-hidden="true"></i>
    </button>
    <div class="pull-right" >
      <button class="btn btn-warning" id="btn-export-project" >
          <i class="fa fa-file-excel-o" aria-hidden="true"></i>
      </button>
      <button class="btn btn-primary" id="btn-refresh-project" >
        <i class="fa fa-refresh" aria-hidden="true"></i>
      </button>
      <button class="btn btn-success" data-toggle="modal" data-target="#edit-project-name" >
        <span class="glyphicon glyphicon-pencil" ></span>
      </button>
    

      <button class="btn btn-danger" id="btn-delete-project" >
        <span class="glyphicon glyphicon-trash" ></span>
      </button>
      <button class="btn btn-default" id="btn-duplicate-project" >
         <i class="fa fa-files-o" aria-hidden="true"></i>
      </button>
    </div>
  </div>
</div>
<div class="row" style="background-color: #eee;padding: 12px;display: none;margin: 4px;position: relative;" id="channel-info-panel" >
  <div id="channel-info-loader" style="display: none;" ><div style="width: 30px;height: 30px;position: relative;margin-left: -15px;left: 50%;margin-top: -15px;top: 50%;" ><i class="fa fa-circle-o-notch fa-spin" aria-hidden="true"></i></div></div>
  <div class="col-md-4" >
      <div class="form-group">
        <label for="channel-tp1">Talking point 1:</label>
        <input type="text" class="form-control" id="channel-tp1" name="channel-tp1" placeholder="Talking Point 1" >
      </div>
      <div class="form-group">
        <label for="channel-tp2">Talking point 2:</label>
        <input type="text" class="form-control" id="channel-tp2" name="channel-tp2" placeholder="Talking Point 2" >
      </div>
      <div class="form-group">
        <label for="channel-tp3">Talking point 3:</label>
        <input type="text" class="form-control" id="channel-tp3" name="channel-tp3" placeholder="Talking Point 3" >
      </div>
       <div class="form-group">
        <label for="channel-cost-rate">Estemated conversion rate:</label>
        <input type="text" class="form-control" id="channel-cost-rate" name="channel-cost-rate" placeholder="Estemated Conversion Rate" >
      </div>
       <div class="form-group">
       <label for="channel-sale-profit">Estemated profit per sale:</label>
        <input type="text" class="form-control" id="channel-sale-profit" name="channel-sale-profit" placeholder="Per Sale Profit" >
      </div>
   
     
  </div>
  <div class="col-md-8" >
      <div class="form-group">
        <label for="channel-offer-url">Offer url:</label>
        <input type="text" class="form-control" id="channel-offer-url" name="channel-offer-url" placeholder="Offer Url" >
      </div>

      <div class="form-horizontal">
        <div class="form-group">
          <label class="control-label col-sm-4" for="email">Combined Average Views:</label>
          <div class="col-sm-6">
            <p class="form-control-static" id="channel-combined-average-view" ></p>
          </div>
        </div>
         <div class="form-group">
          <label class="control-label col-sm-4" for="email">Combined Cost:</label>
          <div class="col-sm-6">
            <p class="form-control-static" id="channel-cost" ></p>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-4" for="email">Estimated Sales:</label>
          <div class="col-sm-6">
            <p class="form-control-static" id="estimated-sales" ></p>
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-sm-4" for="email">Estimated Gross:</label>
          <div class="col-sm-6">
            <p class="form-control-static" id="estimated-gross" ></p>
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-sm-4" for="email">Estimated Net:</label>
          <div class="col-sm-6">
            <p class="form-control-static" id="estimated-net" ></p>
          </div>
        </div>

      </div>
  </div>
  <div class="col-md-12" >
      <buton class="btn btn-success pull-right" id="btn-save-project-info" >Save</buton>
  </div>
</div>
<div class="row">
  <div class="col-md-12" >
    <table class="table table-striped" id="tbl-list" style="display: none;" >
        <thead>
            <tr>
              <th style="width: 40px;">
                <label class="checkbox-inline"><input type="checkbox" style="margin-bottom: 10px;position: relative;left: 0;" class="parent-item-check" value=""></label>
               <!--  <button class="btn btn-default" style="float: right;" >Go</button> -->
                <!-- <select class="form-control" id="sel1" style="width: 81px;float: right;right: 8px;position: relative;" >
                  <option value="default" > - - </option>
                  <option value="update" >Update</option>
                  <option value="deactive" >Activate</option>
                  <option value="deactived" >Deactivate</option>
                </select> -->

              </th>
              <th></th>
              <th>Title</th>
              <th>Email</th>
              <th>No. Videos</th>
              <th>Ave. Views</th>
              <th>Cost</th>
              <th></th>
              <th></th>
              <th class="hidden"></th>
              <th></th>
              <th></th>
              <th></th>
            </tr>
        </thead>
        <tbody id="tbl-list-body" >
        </tbody>
      </table>
  </div>
</div>
<div id="edit-project-name" class="modal fade" role="dialog">
    <div class="modal-dialog">

    <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit Project Name</h4>
          </div>
          <div class="modal-body">
          <div class="form-group">
            <input type="text" class="form-control" id="project-new-name">
        </div>    
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="btn-edit-project" >Save</button>
          </div>
      </div>

  </div>
</div>