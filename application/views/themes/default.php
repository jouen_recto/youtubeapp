<html lang="en">
	<head>
		<title>Youtube App | <?php echo $page_title; ?></title>
		<meta name="resource-type" content="document" />
		<meta name="robots" content="all, index, follow"/>
		<meta name="googlebot" content="all, index, follow" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" type="text/css" />		
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/css/bootstrap3/bootstrap-switch.min.css">
		<link rel="stylesheet" href="<?= base_url(); ?>assets/vendor/css/notie.min.css">
		<link rel="stylesheet" href="<?= base_url(); ?>assets/css/global/main.css">
		<?php
			foreach($css as $file){
			 	echo "\n\t\t";
				?><link rel="stylesheet" href="<?php echo $file; ?>" type="text/css" /><?php
			} echo "\n\t";

		?>
		<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/vendor/datatable/css/datatable-custom.css">

	</head>

  	<body>
  		<?php
  			$nav = true;
  			if(isset($no_nav))
  			{
  				if($no_nav)
  					$nav =false;
  			}
  		
  		?>

  		<div id="global-loader" style="display: none; position: fixed; width: 100%; height: 100%; z-index: 999999; background-color: rgba(16,16,16,0.7);" ><div style="width: 30px;height: 30px;position: relative;margin-left: -15px;left: 50%;margin-top: -15px;top: 50%;" ><i class="fa fa-circle-o-notch fa-spin fa-3x" style="color: #ddd;" aria-hidden="true"></i></div></div>

  		<?php if($nav){ ?>
  			<?php if($this->session->userdata('user_data')[0]->id != 0 ){ ?>
			 	<nav class="navbar navbar-inverse">
			  		<div class="container-fluid">
				    	<div class="navbar-header">
				      		<a class="navbar-brand" href="<?= base_url(); ?>dashboard">YoutubeApp</a>
				    	</div>
					    <ul class="nav navbar-nav">
				      		<li class="<?= $page_title=='dashboard'? 'active':''; ?>"><a href="<?= base_url(); ?>dashboard">Dashboard</a></li>
				      		
				      		<li class="<?= $page_title=='channel'? 'active':''; ?>">
						        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Search <span class="caret"></span></a>
						        <ul class="dropdown-menu">
						          <li><a href="<?= base_url(); ?>channel/search">Channels</a></li>
						          <li><a href="<?= base_url(); ?>channel/searchVideo">Videos</a></li>
						          
						        </ul>
					      	</li>
				      		<li class="dropdown <?= $page_title=='import'? 'active':''; ?>">
						        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Channel Data <span class="caret"></span></a>
						        <ul class="dropdown-menu">
						          <li ><a href="<?= base_url(); ?>import/url">Import Url</a></li>
						          <li ><a href="<?= base_url(); ?>import/csv">Import CSV</a></li>
						          <li ><a href="<?= base_url(); ?>export/csv">Export CSV</a></li>
						        </ul>
					      	</li>
				      		
				      		<li class="<?= $page_title=='project'? 'active':''; ?>" ><a href="<?= base_url(); ?>project/projectList">View Projects</a></li>
				      		<li class="<?= $page_title=='product'? 'active':''; ?>" ><a href="<?= base_url(); ?>product/">Products</a></li>
				      		<li><button class="btn btn-default" style="color: #7c7c7c;font-weight: bold;margin-top: 13px;padding: 1px 7px;text-decoration: none;"  data-toggle="modal" data-target="#modal-create-project">Create Project</button></li>
					    </ul>
					    <ul class="nav navbar-nav navbar-right">
					      <li class="dropdown">
					        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-user"></span> <?= $this->session->userdata('user_data')[0]->name ?>
					        <span class="caret"></span></a>
					        <ul class="dropdown-menu">
					          <li><a href="<?= base_url() ?>favorite/favoiteList"><span class="badge user-badge-favorite"><?php user_favorite($this->session->userdata('user_data')[0]->id); ?></span> Favorties</a></li>
					          <li><a href="<?= base_url() ?>note/noteList"><span class="badge user-badge-note"><?php user_notes($this->session->userdata('user_data')[0]->id); ?></span> Notes</a></li>
					          <li><a href="<?= base_url() ?>logout">Logout</a></li>
					        </ul>
					      </li>
					    </ul>
				  	</div>
				</nav>
			<?php }else { ?>
				<nav class="navbar navbar-inverse">
			  		<div class="container-fluid">
				    	<div class="navbar-header">
				      		<a class="navbar-brand" href="<?= base_url(); ?>dashboard">YoutubeApp</a>
				    	</div>
					    
					    <ul class="nav navbar-nav navbar-right">
					      <li class="dropdown">
					        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-user"></span> <?= $this->session->userdata('user_data')[0]->name ?>
					        <span class="caret"></span></a>
					        <ul class="dropdown-menu">
					          <li><a href="<?= base_url() ?>logout">Logout</a></li>
					        </ul>
					      </li>
					    </ul>
				  	</div>
				</nav>
			<?php } ?>
		<?php } ?> 
    	<div class="container" >
	  		<?php echo $output;?>
	      	<footer>
	      		<script type="text/javascript" src='<?= base_url(); ?>assets/vendor/js/jquery.min.js'></script>
				<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js" type="text/javascript"></script>
				<script type="text/javascript" src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>
				<script type="text/javascript" src='<?= base_url(); ?>assets/vendor/datatable/js/jquery.dataTables.min.js'></script>
				<script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.min.js'></script>
				<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>
				<script type="text/javascript" src='<?= base_url(); ?>assets/vendor/js/notie.min.js'></script>
				<script type="text/javascript"> var base_url = "<?php echo base_url(); ?>"; </script>
				<script type="text/javascript" src='<?= base_url(); ?>assets/js/global/main.js'></script>
				
		      	<?php
		      		foreach($js as $file){
							echo "\n\t\t";
							?><script src="<?php echo $file; ?>"></script><?php
					} echo "\n\t";
		      	?>
	      	</footer>
	    </div>
	    <div id="modal-create-project" class="modal fade" role="dialog">
  			<div class="modal-dialog">
    			<div class="modal-content">
      				<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal">&times;</button>
    					<h4 class="modal-title">Create Project</h4>
      				</div>
      				<div class="modal-body">
      					<div id="container-response-new-project" ></div>
        				<form id="create-project-form">
        					<div class="form-group">
  								<input type="text" class="form-control" id="new-project-title" name="new-project-title" placeholder="Project Title*">
							</div>
        				</form>
      				</div>
      				<div class="modal-footer">
        				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        				<button type="button" class="btn btn-primary" id="btn-save-new-project" >Save</button>
      				</div>
    			</div>

  			</div>
		</div>
	</body>
</html>
