$(document).ready(function(){
	$("#btn-sync").click(function(){
    	$("#channel-loader").show();
    	$.ajax({
			type:'get',
			url:base_url+"channel/get_data_top_5000",
			success:function(data){

				//console.table(JSON.parse(data));
				var channel_data = [];
				var parent = $(data);
				parent.find('.channel-data').each(function(){
					
					var url = $(this).find('.channel-id').html();
					var name = $(this).find('.channel-name').html();

					channel_data.push([url,name]);
					
				});
				
				$(".channels").val(JSON.stringify(channel_data));
				$(".form-sync-to-load").each(function(){
					$(this).submit();
				});
				
				
			}
		});
    });
});