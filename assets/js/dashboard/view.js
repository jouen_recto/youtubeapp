function favorite(channel,user,btn)
{
	$(btn).addClass('disabled');
	$.ajax({
		"url":base_url+'favorite/channel',
		'type':'post',
		data:{channel:channel,user:user},
		success:function(data)
		{
			$(btn).removeClass('btn-default');
			$(btn).removeClass('btn-primary');
			$(btn).removeClass('disabled');
			$(btn).addClass(data);

			var favorite_num = parseInt($(".user-badge-favorite").html());
			if(data != "btn-default")
				$(".user-badge-favorite").html(favorite_num+1);
			else
				$(".user-badge-favorite").html(favorite_num-1);
		},
	});
	
}

function saveEmailChannel(channel_id,type = 'first-email')
{
	notie.alert({type:'warning',text:'Saving...',stay:true});

	if(type == 'first-email')
		var email = $("#channel-email-text").val();
	else
		var email = $("#channel-second-email-text").val();

	$.ajax({
		url:base_url+'channel/saveEmail',
		type:'post',
		data:{email:email,channel_id:channel_id,type:type},
		success:function(data){
			notie.alert({type:'success',text:'Email successfully saved.',stime:3});
			
		}
	});
}

function saveCostChannel(channel_id)
{
	notie.alert({type:'warning',text:'Saving...',stay:true});
	var cost = $("#channel-cost-text").val();
	$.ajax({
		url:base_url+'channel/saveCost',
		type:'post',
		data:{cost:cost,channel_id:channel_id},
		success:function(data){
			notie.alert({type:'success',text:'Cost successfully saved.',stime:3});
			
		}
	});
}

$(document).ready(function(){
	$("#top-20-video-table").dataTable( {
	    "paging": false,
	    "searching": false,
	    "bInfo": false,
	    "aaSorting":[[0,'asc']]
	} );
	$("#top-10-video-table").dataTable( {
	    "paging": false,
	    "searching": false,
	    "bInfo": false,
	    "aaSorting":[[0,'asc']]
	} );

	$("#save-user-note").click(function(){
		var id = $("#note-id").val();
		var note = $("#user-notes").val();
		var channel = $("#channel-note-id").val();
		var url = base_url+"note/add";
		if(id != 0)
			url = base_url+"note/update";

		$("#global-loader").show();
		$("body").addClass('noscroll');
		$.ajax({
			url:url,
			type:'post',
			data:{id:id,note:note,channel:channel},
			success:function(data){
				$("#global-loader").hide();
				$("body").removeClass('noscroll');
				$("#note-id").val(data);
				var msg = '<div class="alert alert-success"><strong>Success!</strong> Note successfully saved.</div>';
				$(".note-msg-container").html(msg);

				var note_num = parseInt($(".user-badge-note").html());
				if(url == (	base_url+"note/add"))
					$(".user-badge-note").html(note_num+1);
			}
		});


	});

	$('#note-modal').on('hidden.bs.modal', function () {
	    $(".note-msg-container").html('');
	})
});