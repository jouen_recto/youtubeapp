$(function() {
     var ajax_scroll = true;
     window.onscroll = function(ev) {
         if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
               if($("#search-status").val() == '1')
               {
                    var limit = parseInt($("#search-limit").val());
                    var offset = parseInt($("#search-offset").val());
                    if(offset >= limit)
                    {

                         if(ajax_scroll)
                         {
                              ajax_scroll = false;
                              $("#search-load-bar").show();

                              var search_data = new FormData(document.getElementById("search-form-data"));

                              if($("#search-video").val() != 'search-video')
                              {
                                   $.ajax({
                                        url:base_url+'channel/loadChannelresult',
                                        type:'post',
                                        data: search_data,
                                        processData: false,
                                        contentType: false,
                                        dataType:'json',
                                        success:function(data)
                                        {
                                           
                                             ajax_scroll = true;
                                             $("#search-load-bar").hide();
                                             $("#search-offset").val(data.size);
                                             
                                             data.channel.forEach(function(index){

                                                  var img="";
                                                  if(index.thumbnails != "")
                                                  {
                                                       img = JSON.parse(index.thumbnails).default.url;
                                                  }
                                                  
                                                  var parent_div = $('<div class="col-md-3" ></div>');
                                                  var card_container = $('<div class="channel-card-container" ></div>');
                                                  var a = $('<a href="'+base_url+'dashboard/view/'+index.id+'" ></a>');
                                                  var card_min = $('<div class="card-min" ></div>');
                                                  var img = $('<img src="'+img+'" />');

                                                  if(index.title.length > 15)
                                                  {
                                                       title = index.title.substring(0,15)+'...';
                                                  }else
                                                  {
                                                       title = index.title;
                                                  }

                                                  var h3 = $('<h3>'+title+'</h3>');

                                                  if(index.description.length > 20)
                                                  {
                                                       description = index.description.substring(0,20)+'...';
                                                  }else
                                                  {
                                                       description = index.description;
                                                  } 
                                                  var p = $('<p class="card-wrap" >'+description+'</p>');
                                                  var ul = $('<ul class="list-group" ></ul>');

                                                  if(index.email.length > 20)
                                                  {
                                                       email = index.email.substring(0,20)+'...';
                                                  }else
                                                  {
                                                       email = index.email;
                                                  }

                                                  if(index.insta)
                                                  {
                                                       var insta = index.insta.username
                                                  }else
                                                  {
                                                       var insta = "None";
                                                  }

                                                  var li_email = $('<li class="list-group-item"><b>Email</b> '+email+'</li>');
                                                  var li_country = $('<li class="list-group-item"><b>Country</b> '+index.country+'</li>');
                                                  var li_views = $('<li class="list-group-item"><b>Views</b> '+index.viewCount+'</li>');
                                                  var li_subscriber = $('<li class="list-group-item"><b>Subscriber</b> '+index.subscriberCount+'</li>');
                                                  var li_videos = $('<li class="list-group-item"><b>Videos</b> '+index.videoCount+'</li>');
                                                  var li_ave = $('<li class="list-group-item"><b>Ave. Views</b> '+index.ave_view+'</li>');
                                                  var li_insta = $('<li class="list-group-item"><b>Instagram</b> '+insta+'</li>');

                                                  //append header card
                                                  card_min.append(img);
                                                  card_min.append(h3);
                                                  card_min.append(p);

                                                  //append info of card
                                                  ul.append(li_email);
                                                  ul.append(li_country);
                                                  ul.append(li_views);
                                                  ul.append(li_subscriber);
                                                  ul.append(li_videos);
                                                  ul.append(li_ave);
                                                  ul.append(li_insta);

                                                  //contain in a tag
                                                  a.append(card_min);
                                                  a.append(ul);

                                                  //appned to card container
                                                  card_container.append(a);

                                                  //append on parent
                                                  parent_div.append(card_container);

                                                  $("#container-result").append(parent_div);



                                             });
                                             //$("#container-result").append(data.channel);
                                        }
                                   });

                              }else
                              {
                                   $.ajax({
                                        url:base_url+'channel/loadVideoresult',
                                        type:'post',
                                        data: search_data,
                                        processData: false,
                                        contentType: false,
                                        dataType:'json',
                                        success:function(data)
                                        {
                                           
                                             ajax_scroll = true;
                                             $("#search-load-bar").hide();
                                             $("#search-offset").val(data.size);
                                             
                                             data.video.forEach(function(index){

                                                  // var img="";
                                                  // if(index.thumbnails != "")
                                                  // {
                                                  //      img = JSON.parse(index.thumbnails).default.url;
                                                  // }
                                                  
                                                  var parent_div = $('<div class="col-md-3" ></div>');
                                                  var card_container = $('<div class="channel-card-container" ></div>');
                                                  var a = $('<a href="'+base_url+'dashboard/view/'+index.channelId+'" ></a>');
                                                  var card_min = $('<div class="card-min" ></div>');
                                                  // var img = $('<img src="'+img+'" />');

                                                  // if(index.title.length > 15)
                                                  // {
                                                  //      title = index.title.substring(0,15)+'...';
                                                  // }else
                                                  // {
                                                  //      title = index.title;
                                                  // }

                                                  // var h3 = $('<h3>'+title+'</h3>');

                                                  // if(index.description.length > 20)
                                                  // {
                                                  //      description = index.description.substring(0,20)+'...';
                                                  // }else
                                                  // {
                                                  //      description = index.description;
                                                  // } 
                                                  var p = $('<p class="card-wrap" >'+index.title+'</p>');
                                                  var ul = $('<ul class="list-group" ></ul>');

                                                  // if(index.email.length > 20)
                                                  // {
                                                  //      email = index.email.substring(0,20)+'...';
                                                  // }else
                                                  // {
                                                  //      email = index.email;
                                                  // }

                                                  var li_view = $('<li class="list-group-item"><b>Views</b> '+index.videCount+'</li>');
                                                  // var li_country = $('<li class="list-group-item"><b>Country</b> '+index.country+'</li>');
                                                  // var li_views = $('<li class="list-group-item"><b>Views</b> '+index.viewCount+'</li>');
                                                  // var li_subscriber = $('<li class="list-group-item"><b>Subscriber</b> '+index.subscriberCount+'</li>');
                                                  // var li_videos = $('<li class="list-group-item"><b>Videos</b> '+index.videoCount+'</li>');
                                                  // var li_ave = $('<li class="list-group-item"><b>Ave. Views</b> '+index.ave_view+'</li>');

                                                  //append header card
                                                  // card_min.append(img);
                                                  // card_min.append(h3);
                                                  card_min.append(p);

                                                  //append info of card
                                                  ul.append(li_view);
                                                  // ul.append(li_country);
                                                  // ul.append(li_views);
                                                  // ul.append(li_subscriber);
                                                  // ul.append(li_videos);
                                                  // ul.append(li_ave);

                                                  //contain in a tag
                                                  a.append(card_min);
                                                  a.append(ul);

                                                  //appned to card container
                                                  card_container.append(a);

                                                  //append on parent
                                                  parent_div.append(card_container);

                                                  $("#container-result").append(parent_div);



                                             });
                                             //$("#container-result").append(data.channel);
                                        }
                                   });
                              }
                         }
                    }
               }
             
         }
     };

     jQuery("#channel-name").autocomplete({
          source: function(request, response) {
          //console.log(request.term);
               var sqValue = [];
               jQuery.ajax({
                    type: "POST",
                    url: base_url+"channel/autocomplete",
                    dataType: 'json',
                    data: jQuery.extend({
                         q: request.term
                    }, {}),
                    success: function(data) {
                         
                         obj = data;
                         jQuery.each(obj, function(key, value) {
                              sqValue.push(value);
                         });
                         response(sqValue);
                    }
               });
          },
          select: function(e, ui) {
               e.preventDefault();
               $(this).val(ui.item.label);
               // $("#result-table").show();
               // $.ajax({
               //      url:base_url+'channel/getChannelData',
               //      type:'post',
               //      data:{channelId:ui.item.value},
               //      dataType:'json',
               //      success:function(data){
               //           $('#result-table-body').html('');

               //           var button_add;
               //           var button_favorite = $("<span></span>");
               //           var button_project = $("<span></span>");
               //           if(data.is_save)
               //           {
               //                var class_favorite = "default";
               //                if(data.is_favorite)
               //                     class_favorite = "primary";

               //                button_favorite = $("<button class='btn btn-"+class_favorite+"' data-channel-id='"+data.channel_id+"' data-user-id='"+data.user_id+"' ><span class='glyphicon glyphicon-thumbs-up' ></span></button>");
               //                var button_ = $('<button class="btn btn-primary dropdown-toggle " onclick="loadProject(\''+data.channel_id+'\',this);" type="button" data-toggle="dropdown">Add to Project <span class="caret"></span></button>');
               //                button_.click(function(e){
               //                     var parent = $(this).parent();

               //                     if(parent.hasClass('open'))
               //                          parent.removeClass('open');
               //                     else
               //                          parent.addClass('open');

               //                     e.stopPropagation();
               //                });
               //                button_project = $('<div class="dropdown pull-right"><ul class="dropdown-menu"><li class="project-spinner"><a href="javascript:void(0);" style="text-align: center;"><i class="fa fa-spinner fa-spin" aria-hidden="true" ></i></a></li></ul></div>');
               //                button_project.prepend(button_);

               //                button_favorite.click(function(e){
               //                     e.stopPropagation();
               //                     var channel_id = $(this).data('channel-id');
               //                     var user_id = $(this).data('user-id');
               //                     var btn = $(this);
               //                     $(this).addClass('disabled');

               //                     $.ajax({
               //                          "url":base_url+'favorite/channel',
               //                          'type':'post',
               //                          data:{channel:channel_id,user:user_id},
               //                          success:function(data)
               //                          {

               //                               btn.removeClass('btn-default');
               //                               btn.removeClass('btn-primary');
               //                               btn.removeClass('disabled');
               //                               btn.addClass(data);

               //                               var favorite_num = parseInt($(".user-badge-favorite").html());
               //                               if(data != "btn-default")
               //                                    $(".user-badge-favorite").html(favorite_num+1);
               //                               else
               //                                    $(".user-badge-favorite").html(favorite_num-1);
                                             
               //                          },
               //                     });
               //                });
               //                button_add ="<p>Already Add in Database</p>";
               //           }else
               //           {
                              
               //                button_add =$("<button data-channel-id='"+data.id+"' class='btn btn-primary' >Add to Database</button>");
               //                button_add.click(function(e){
               //                     e.stopPropagation();
               //                     var id = $(this).attr('data-channel-id');
               //                     $.ajax({
               //                          url:base_url+"channel/addChannel",
               //                          type:'post',
               //                          data:{channelId:id},
               //                          dataType:'json',
               //                          success:function(data){  
               //                               if(data.status)
               //                               {
               //                                    $("#result-table").hide();
               //                                    $('#result-table-body').html('');
               //                                    alert(data.message);
               //                               }
               //                          }
               //                     });
               //                });
               //           }

               //           var tr = $("<tr data-channel-url='"+data.url+"'class='tr-link' data-channel-id='"+data.id+"' ></tr>");
               //           tr.click(function(){
               //                var url = $(this).data("channel-url");

               //                if(url)
               //                {
               //                     window.open($(this).data("channel-url"),'_blank');     
               //                }else
               //                {
               //                     var con = confirm('this channel is not yet added in database. Click \'ok\' to add and view the channel.');

               //                     if(con)
               //                     {
               //                          var id = $(this).attr('data-channel-id');
                                        
               //                          $.ajax({
               //                               url:base_url+"channel/addChannel",
               //                               type:'post',
               //                               data:{channelId:id},
               //                               dataType:'json',
               //                               success:function(data){  
               //                                    if(data.status)
               //                                    {
               //                                         $("#result-table").hide();
               //                                         $('#result-table-body').html('');
               //                                         window.open(base_url+'dashboard/view/'+data.channelId,'_blank');
               //                                         $('#hyv-search').data('ui-autocomplete')._trigger('select', 'autocompleteselect', {item:{value:id}});

               //                                    }
               //                               }
               //                          });
               //                     }
               //                }
                              
               //           });
               //           tr.append($('<td>'+data.title+'<br/></td>').append(button_favorite).append(button_project));
               //           tr.append("<td><img src='"+data.img+"' /></td>");
               //           tr.append('<td>'+data.subscriber+'</td>');
               //           tr.append('<td>'+data.view+'</td>');
               //           tr.append('<td>'+data.video+'</td>');
               //           tr.append($('<td></td>').append(button_add));
               //           tr.append('<td>'+data.ave_view+'</td>');

               //           $('#result-table-body').append(tr);
               //      }
               // });

          },
          // focus: function( e, ui ) {
          //       e.preventDefault();
               
          //      $(this).val(ui.item.label);
          // }

     });

     $("#form-search-channel").submit(function(){

          var is_empty = true;
          var msg = "You should put a value to any option to serach a channel";
          if($("#channel-name").val() != "" )
               is_empty = false;

          if($("#channel-category").val() != "default")
               is_empty = false;

          if($("#channel-country").val() != "default")
               is_empty = false;

          if($("#channel-subscriber-max").val() != "" && $("#channel-subscriber-min").val() != "")
          {
               if(parseInt($("#channel-subscriber-max").val()) > parseInt($("#channel-subscriber-min").val()) )
                    is_empty = false;
               else{
                    is_empty = true;
                    msg = "the value of MAX is less than in MIN for Subscriber"
               }
          }

          if($("#channel-view-max").val() != "" && $("#channel-view-min").val() != "")
          {
               if(parseInt($("#channel-view-max").val()) > parseInt($("#channel-view-min").val()) )
                    is_empty = false;
                else{
                    is_empty = true;
                    msg = "the value of MAX is less than in MIN for Average View"
                }
          }

          if(is_empty)
          {
               $("#form-search-error-log").html('<div class="alert alert-danger"><strong>Error!</strong> '+msg+'.</div>')
               return false;
          }



     });

     $("#form-search-video").submit(function(){

          var is_empty = true;
          var msg = "You should put a value to any option to serach a channel";
          if($("#video-name").val() != "" )
               is_empty = false;

          

          if($("#video-view-max").val() != "" && $("#video-view-min").val() != "")
          {
               if(parseInt($("#video-view-max").val()) > parseInt($("#video-view-min").val()) )
                    is_empty = false;
                else{
                    is_empty = true;
                    msg = "the value of MAX is less than in MIN for Average View"
                }
          }

          if(is_empty)
          {
               $("#form-search-error-log").html('<div class="alert alert-danger"><strong>Error!</strong> '+msg+'.</div>')
               return false;
          }



     });

    
});


function saveChannel(id)
{
    
     $.ajax({
          url:base_url+"channel/addChannel",
          type:'post',
          data:{channelId:id},
          dataType:'json',
          success:function(data){  
               if(data.status)
               {
                    $("#result-table").hide();
                    $('#result-table-body').html('');
                    alert(data.message);
               }
          }
     });
}

// function favorite(channel,user,btn)
// {
//      $(btn).addClass('disabled');
//      $.ajax({
//           "url":base_url+'favorite/channel',
//           'type':'post',
//           data:{channel:channel,user:user},
//           success:function(data)
//           {
//                $(btn).removeClass('btn-default');
//                $(btn).removeClass('btn-primary');
//                $(btn).removeClass('disabled');
//                $(btn).addClass(data);

//                var favorite_num = parseInt($(".user-badge-favorite").html());
//                if(data != "btn-default")
//                     $(".user-badge-favorite").html(favorite_num+1);
//                else
//                     $(".user-badge-favorite").html(favorite_num-1);
               
//           },
//      });
     
// }