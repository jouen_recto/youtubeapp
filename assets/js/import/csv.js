$(document).ready(function(){
	$("#csv-file").change(function(){
		if($(this).val().split('.').pop() == "csv")
		{
			$("#loader-icon-import").show();
			$("#import-csv-form").submit();
		}else
		{
			notie.alert({type:'error',text:'The file you selected is not a scv format',time:2}	);
		}
	});

	$("#load-import-csv").load(function(){
		$("#loader-icon-import").hide();
		notie.alert({type:'success',text:'Data successfully saved.',time:2});
	});
});